(function() {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _src_App_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../src/App.js */ "./src/App.js");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\pages\\index.js";




const index = () => {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_src_App_js__WEBPACK_IMPORTED_MODULE_2__.default, {
    pageName: "Home"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 12
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (index);

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SignUp__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SignUp */ "./src/SignUp.js");
/* harmony import */ var _Login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Login */ "./src/Login.js");
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Dashboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Dashboard */ "./src/Dashboard.js");
/* harmony import */ var _PrivateRoute__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./PrivateRoute */ "./src/PrivateRoute.js");
/* harmony import */ var _ForgotPassword__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ForgotPassword */ "./src/ForgotPassword.js");
/* harmony import */ var _UpdateProfile__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./UpdateProfile */ "./src/UpdateProfile.js");
/* harmony import */ var _GameDetail__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./GameDetail */ "./src/GameDetail.js");
/* harmony import */ var _Home__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Home */ "./src/Home.js");

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\App.js";












const App = () => {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "w-100",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.BrowserRouter, {
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__.AuthProvider, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Switch, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Route, {
            exact: true,
            path: "/",
            component: _Home__WEBPACK_IMPORTED_MODULE_11__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 20,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_PrivateRoute__WEBPACK_IMPORTED_MODULE_7__.default, {
            path: "/update-profile",
            component: _UpdateProfile__WEBPACK_IMPORTED_MODULE_9__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 21,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_PrivateRoute__WEBPACK_IMPORTED_MODULE_7__.default, {
            path: "/dashboard",
            component: _Dashboard__WEBPACK_IMPORTED_MODULE_6__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 22,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Route, {
            path: "/signup",
            component: _SignUp__WEBPACK_IMPORTED_MODULE_2__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 23,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Route, {
            path: "/login",
            component: _Login__WEBPACK_IMPORTED_MODULE_3__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Route, {
            path: "/forgot-password",
            component: _ForgotPassword__WEBPACK_IMPORTED_MODULE_8__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 25,
            columnNumber: 33
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Route, {
            path: "/matches",
            component: _GameDetail__WEBPACK_IMPORTED_MODULE_10__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 26,
            columnNumber: 33
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 19,
          columnNumber: 29
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 25
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 22
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 17
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/Dashboard.js":
/*!**************************!*\
  !*** ./src/Dashboard.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Dashboard; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\Dashboard.js";






function Dashboard() {
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    currentUser,
    logout
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__.useAuth)();
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header__WEBPACK_IMPORTED_MODULE_5__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 9
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu__WEBPACK_IMPORTED_MODULE_6__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__.Container, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__.Card, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__.Card.Body, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
            children: "Profile"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 18,
            columnNumber: 21
          }, this), error && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Alert, {
            children: error
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 31
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("strong", {
              children: "Email: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 20,
              columnNumber: 26
            }, this), currentUser.email]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 20,
            columnNumber: 21
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("strong", {
              children: "Nickname: "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 21,
              columnNumber: 26
            }, this), currentUser.displayName]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 21,
            columnNumber: 21
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__.Button, {
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_4__.Link, {
              to: "/update-profile",
              className: "btn w-100",
              children: "Update Profile"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 23,
              columnNumber: 25
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 22,
            columnNumber: 21
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 17,
          columnNumber: 17
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 13
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "w-100 text-center mt-2"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 13
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }, this)]
  }, void 0, true);
}

/***/ }),

/***/ "./src/ForgotPassword.js":
/*!*******************************!*\
  !*** ./src/ForgotPassword.js ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ ForgotPassword; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_6__);


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\ForgotPassword.js";






function ForgotPassword() {
  const emailRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const {
    resetPassword
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__.useAuth)();
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: message,
    1: setMessage
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      setMessage("");
      setError("");
      setLoading(true);
      await resetPassword(emailRef.current.value);
      setMessage("Check your email");
    } catch {
      setError("Failed to reset password");
    }

    setLoading(false);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card, {
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card.Body, {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
          className: "text-center mb-4",
          children: "Password reset"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 9
        }, this), error && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Alert, {
          style: {
            color: "red"
          },
          children: error
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 19
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form, {
          onSubmit: handleSubmit,
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
            id: "email",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
              children: "Email"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 36,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
              type: "email",
              ref: emailRef,
              required: true
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 13
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 11
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Button, {
            disabled: loading,
            className: "w-100",
            type: "submit",
            children: "Reset Password"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 11
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 9
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
            to: "/login",
            children: "Login"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 9
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 7
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 5
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "w-100 text-center mt-2",
      children: ["Dont have an account? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
        to: "/signup",
        children: "Sign Up!"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 29
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 5
    }, this)]
  }, void 0, true);
}

/***/ }),

/***/ "./src/GameDetail.js":
/*!***************************!*\
  !*** ./src/GameDetail.js ***!
  \***************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var _fire_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./fire.js */ "./src/fire.js");
/* harmony import */ var _Header_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Header.js */ "./src/Header.js");
/* harmony import */ var _Menu_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Menu.js */ "./src/Menu.js");
/* harmony import */ var _PredictionForm_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./PredictionForm.js */ "./src/PredictionForm.js");


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\GameDetail.js";








const GameDetail = () => {
  const {
    currentUser,
    logout
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__.useAuth)();
  const {
    0: games,
    1: setGames
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({});
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);

  const getMatchData = () => {
    _fire_js__WEBPACK_IMPORTED_MODULE_4__.db.ref("matches/").orderByChild("dateTime").on("value", snapshot => {
      setGames(snapshot.val()); //console.log(snapshot.val())

      setLoading(true);
    });
    return () => _fire_js__WEBPACK_IMPORTED_MODULE_4__.db.ref.off();
  };

  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    getMatchData();
  }, []);

  if (!loading) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      children: "Loading"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 13
    }, undefined);
  } else {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header_js__WEBPACK_IMPORTED_MODULE_5__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 13
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu_js__WEBPACK_IMPORTED_MODULE_6__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 13
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__.Container, {
        style: {
          display: 'inherit'
        },
        children: Object.keys(games).map((item, i) => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "card-body",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h4", {
              className: "card-title",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                  className: "flag",
                  src: ""
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 42,
                  columnNumber: 29
                }, undefined), " ", games[item].homeTeam, " - ", games[item].awayTeam, " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
                  className: "flag",
                  src: ""
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 42,
                  columnNumber: 108
                }, undefined)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 41,
                columnNumber: 25
              }, undefined)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 40,
              columnNumber: 25
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
              children: new Date(games[item].dateTime * 1000).toLocaleString()
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 17
            }, undefined), !games[item].finished && currentUser && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_PredictionForm_js__WEBPACK_IMPORTED_MODULE_7__.default, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 47,
              columnNumber: 13
            }, undefined)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 21
          }, undefined)
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 17
        }, undefined))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 13
      }, undefined)]
    }, void 0, true);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (GameDetail);

/***/ }),

/***/ "./src/Header.js":
/*!***********************!*\
  !*** ./src/Header.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\Header.js";




const Header = () => {
  const {
    currentUser,
    logout
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__.useAuth)();
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    className: "jumbotron jumbotronheight",
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "row",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "col-12 col-sm-10 text-lg-right",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
              src: "/static/logot/UEFA-Euro-2020.png"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 14,
              columnNumber: 29
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 13,
            columnNumber: 25
          }, undefined), currentUser && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h5", {
            children: ["Logged in as: ", currentUser.displayName]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 41
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 12,
          columnNumber: 21
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 17
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 13
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 9
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/Home.js":
/*!*********************!*\
  !*** ./src/Home.js ***!
  \*********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\Home.js";




const index = () => {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 13
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 13
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "container",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "row",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h3", {
          className: "",
          children: "Welcome to the Euro2020 score prediction app."
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 12,
          columnNumber: 21
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 17
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 13
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 9
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (index);

/***/ }),

/***/ "./src/Login.js":
/*!**********************!*\
  !*** ./src/Login.js ***!
  \**********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_6__);


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\Login.js";







const Login = () => {
  const emailRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const passwordRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    login
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__.useAuth)();
  const history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_6__.useHistory)();

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      setError("");
      setLoading(true);
      await login(emailRef.current.value, passwordRef.current.value);
      history.push("/");
    } catch {
      setError("Failed to log in");
    }

    setLoading(false);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Container, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card.Body, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
            className: "text-center mb-4",
            children: "Log In"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 9
          }, undefined), error && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Alert, {
            style: {
              color: "red"
            },
            children: error
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 19
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form, {
            onSubmit: handleSubmit,
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "email",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Email"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 39,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "email",
                ref: emailRef,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 40,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 38,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "password",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Password"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 43,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "password",
                ref: passwordRef,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 44,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 42,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Button, {
              disabled: loading,
              className: "w-100",
              type: "submit",
              children: "Log In"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 46,
              columnNumber: 11
            }, undefined)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 9
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
              to: "/forgot-password",
              children: "Forgot Password?"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 49,
              columnNumber: 13
            }, undefined)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 48,
            columnNumber: 9
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 7
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 5
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "w-100 text-center mt-2",
        children: ["Dont have an account? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
          to: "/signup",
          children: "Sign Up!"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 54,
          columnNumber: 29
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 5
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 5
    }, undefined)]
  }, void 0, true);
};

/* harmony default export */ __webpack_exports__["default"] = (Login);

/***/ }),

/***/ "./src/Menu.js":
/*!*********************!*\
  !*** ./src/Menu.js ***!
  \*********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\Menu.js";




const Menu = () => {
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    currentUser,
    logout
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__.useAuth)();
  const history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_3__.useHistory)();

  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to logout");
    }
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("nav", {
    style: {
      display: 'inline-block'
    },
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_3__.Link, {
            to: "/",
            children: "Home"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 25
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_3__.Link, {
            to: "/matches",
            children: "Matches"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 29,
            columnNumber: 25
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 21
        }, undefined), currentUser && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_3__.Link, {
            to: "/dashboard",
            children: "Dashboard"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 25
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 21
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          children: [!currentUser && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_3__.Link, {
            to: "/login",
            children: "Login"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 25
          }, undefined), currentUser && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_3__.Link, {
            to: "/",
            onClick: handleLogout,
            children: "Logout"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 25
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 21
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 17
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 13
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 20,
    columnNumber: 9
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (Menu);

/***/ }),

/***/ "./src/PredictionForm.js":
/*!*******************************!*\
  !*** ./src/PredictionForm.js ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\PredictionForm.js";


const PredictionForm = () => {
  function predictionSaveHandler(event) {
    event.preventDefault();
    console.log("Submitted");
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("form", {
      onSubmit: predictionSaveHandler,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        className: "scoreinput",
        type: "number",
        required: true
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 21
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("input", {
        className: "scoreinput",
        type: "number",
        required: true
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 21
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        type: "submit",
        children: "Save"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 21
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 17
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 13
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (PredictionForm);

/***/ }),

/***/ "./src/PrivateRoute.js":
/*!*****************************!*\
  !*** ./src/PrivateRoute.js ***!
  \*****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ PrivateRoute; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\PrivateRoute.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




function PrivateRoute(_ref) {
  let {
    component: Component
  } = _ref,
      rest = _objectWithoutProperties(_ref, ["component"]);

  const {
    currentUser
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__.useAuth)();
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Route, _objectSpread(_objectSpread({}, rest), {}, {
    render: props => {
      return currentUser ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, props), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 38
      }, this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Redirect, {
        to: "/"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 65
      }, this);
    }
  }), void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./src/SignUp.js":
/*!***********************!*\
  !*** ./src/SignUp.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_6__);


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\SignUp.js";







const SignUp = () => {
  const emailRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const passwordRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const passwordConfirmRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    signup
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__.useAuth)();

  async function handleSubmit(e) {
    e.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match");
    }

    try {
      setError("");
      setLoading(true);
      await signup(emailRef.current.value, passwordRef.current.value);
    } catch {
      setError("Failed to sign up");
    }

    setLoading(false);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Container, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card.Body, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
            className: "text-center mb-4",
            children: "Sign Up"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 9
          }, undefined), error && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Alert, {
            style: {
              color: "red"
            },
            children: error
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 38,
            columnNumber: 19
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form, {
            onSubmit: handleSubmit,
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "email",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Email"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 41,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "email",
                ref: emailRef,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 42,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 40,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "password",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Password"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 45,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "password",
                ref: passwordRef,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 46,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 44,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "password-confirm",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Password Confirmation"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 49,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "password",
                ref: passwordConfirmRef,
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 50,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 48,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Button, {
              disabled: loading,
              className: "w-100",
              type: "submit",
              children: "Sign Up"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 52,
              columnNumber: 11
            }, undefined)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 9
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 7
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 5
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "w-100 text-center mt-2",
        children: ["Already have an account? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
          to: "login",
          children: "Log In!"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 32
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 5
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 5
    }, undefined)]
  }, void 0, true);
};

/* harmony default export */ __webpack_exports__["default"] = (SignUp);

/***/ }),

/***/ "./src/UpdateProfile.js":
/*!******************************!*\
  !*** ./src/UpdateProfile.js ***!
  \******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header */ "./src/Header.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Menu */ "./src/Menu.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_6__);


var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\UpdateProfile.js";







const UpdateProfile = () => {
  const emailRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const passwordRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const displayNameRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const passwordConfirmRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
  const {
    0: error,
    1: setError
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const {
    currentUser,
    updatePassword,
    updateEmail,
    updateDisplayName
  } = (0,_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__.useAuth)();
  const history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_6__.useHistory)();

  function handleSubmit(e) {
    e.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match");
    }

    const promises = [];
    setLoading(true);
    setError("");

    if (emailRef.current.value !== currentUser.email) {
      promises.push(updateEmail(emailRef.current.value));
    }

    if (passwordRef.current.value) {
      promises.push(updatePassword(passwordRef.current.value));
    }

    if (displayNameRef.current.value !== currentUser.displayName) {
      promises.push(updateDisplayName(displayNameRef.current.value));
    }

    Promise.all(promises).then(() => {
      history.push("/");
    }).catch(() => {
      setError("Failed to update account");
    }).finally(() => {
      setLoading(false);
    });
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Menu__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 5
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Container, {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card, {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Card.Body, {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
            className: "text-center mb-4",
            children: "Update Profile"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 52,
            columnNumber: 9
          }, undefined), error && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Alert, {
            style: {
              color: "red"
            },
            children: error
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 53,
            columnNumber: 19
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form, {
            onSubmit: handleSubmit,
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "email",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Nickname"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 56,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "text",
                ref: displayNameRef,
                defaultValue: currentUser.displayName
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 57,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 55,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "email",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Email"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 60,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "email",
                ref: emailRef,
                defaultValue: currentUser.email
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 61,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 59,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "password",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Password"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 64,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "password",
                ref: passwordRef,
                placeholder: "Leave blank to keep the same"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 65,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 63,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Group, {
              id: "password-confirm",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Label, {
                children: "Password Confirmation"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 68,
                columnNumber: 13
              }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Form.Control, {
                type: "password",
                ref: passwordConfirmRef,
                placeholder: "Leave blank to keep the same"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 69,
                columnNumber: 13
              }, undefined)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 67,
              columnNumber: 11
            }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__.Button, {
              disabled: loading,
              className: "w-100",
              type: "submit",
              children: "Update"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 71,
              columnNumber: 11
            }, undefined)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 54,
            columnNumber: 9
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 7
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 5
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "w-100 text-center mt-2",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_router_dom__WEBPACK_IMPORTED_MODULE_6__.Link, {
          to: "/",
          children: "Cancel"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 7
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 5
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 5
    }, undefined)]
  }, void 0, true);
};

/* harmony default export */ __webpack_exports__["default"] = (UpdateProfile);

/***/ }),

/***/ "./src/contexts/AuthContext.js":
/*!*************************************!*\
  !*** ./src/contexts/AuthContext.js ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "useAuth": function() { return /* binding */ useAuth; },
/* harmony export */   "AuthProvider": function() { return /* binding */ AuthProvider; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fire__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fire */ "./src/fire.js");

var _jsxFileName = "C:\\Users\\tumet\\emveikkaus\\src\\contexts\\AuthContext.js";


const AuthContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().createContext();
function useAuth() {
  return (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(AuthContext);
}
function AuthProvider({
  children
}) {
  const {
    0: currentUser,
    1: setCurrentUser
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
  const {
    0: loading,
    1: setLoading
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);

  function signup(email, password) {
    return _fire__WEBPACK_IMPORTED_MODULE_2__.auth.createUserWithEmailAndPassword(email, password);
  }

  function login(email, password) {
    return _fire__WEBPACK_IMPORTED_MODULE_2__.auth.signInWithEmailAndPassword(email, password);
  }

  function logout() {
    return _fire__WEBPACK_IMPORTED_MODULE_2__.auth.signOut();
  }

  function resetPassword(email) {
    return _fire__WEBPACK_IMPORTED_MODULE_2__.auth.sendPasswordResetEmail(email);
  }

  function updateEmail(email) {
    return currentUser.updateEmail(email);
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password);
  }

  function updateDisplayName(displayName) {
    return currentUser.updateProfile({
      displayName: displayName
    });
  }

  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    const unsubscribe = _fire__WEBPACK_IMPORTED_MODULE_2__.auth.onAuthStateChanged(user => {
      setCurrentUser(user);
      setLoading(false);
    });
    return unsubscribe;
  }, []);
  const value = {
    currentUser,
    login,
    signup,
    logout,
    resetPassword,
    updateEmail,
    updatePassword,
    updateDisplayName
  };
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(AuthContext.Provider, {
      value: value,
      children: [!loading && children, children]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 56,
    columnNumber: 12
  }, this);
}

/***/ }),

/***/ "./src/fire.js":
/*!*********************!*\
  !*** ./src/fire.js ***!
  \*********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "auth": function() { return /* binding */ auth; },
/* harmony export */   "db": function() { return /* binding */ db; }
/* harmony export */ });
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ "firebase/app");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/auth */ "firebase/auth");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_auth__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/database */ "firebase/database");
/* harmony import */ var firebase_database__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_database__WEBPACK_IMPORTED_MODULE_2__);



var fire;
var firebaseConfig = {
  apiKey: "AIzaSyBI81-7lwj8vN4P1Jz8yANLdTKw9uQNCgc",
  authDomain: "emveikkaus-ed9e4.firebaseapp.com",
  databaseURL: "https://emveikkaus-ed9e4-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "emveikkaus-ed9e4",
  storageBucket: "emveikkaus-ed9e4.appspot.com",
  messagingSenderId: "1059201635540",
  appId: "1:1059201635540:web:a09dad2608cb477a449741",
  measurementId: "G-M7ZLKE0NJF"
};

if (!(firebase_app__WEBPACK_IMPORTED_MODULE_0___default().apps.length)) {
  fire = firebase_app__WEBPACK_IMPORTED_MODULE_0___default().initializeApp(firebaseConfig);
} else {
  fire = firebase_app__WEBPACK_IMPORTED_MODULE_0___default().app(); // if already initialized, use that one
} //const fire = firebase.initializeApp(firebaseConfig);
//export const auth = fire.auth()
//export const db = firebase.database()


const auth = fire.auth();
const db = fire.database();

/* harmony default export */ __webpack_exports__["default"] = (fire);

/***/ }),

/***/ "./node_modules/bootstrap/dist/css/bootstrap.min.css":
/*!***********************************************************!*\
  !*** ./node_modules/bootstrap/dist/css/bootstrap.min.css ***!
  \***********************************************************/
/***/ (function() {



/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = require("firebase/app");;

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/***/ (function(module) {

"use strict";
module.exports = require("firebase/auth");;

/***/ }),

/***/ "firebase/database":
/*!************************************!*\
  !*** external "firebase/database" ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = require("firebase/database");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-bootstrap");;

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-router-dom");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9lbXZlaWtrYXVzL2NsaXAyLVNldHRpbmctVXAtVG9vbGNoYWluLy4vcGFnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9BcHAuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9EYXNoYm9hcmQuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9Gb3Jnb3RQYXNzd29yZC5qcyIsIndlYnBhY2s6Ly9lbXZlaWtrYXVzL2NsaXAyLVNldHRpbmctVXAtVG9vbGNoYWluLy4vc3JjL0dhbWVEZXRhaWwuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9IZWFkZXIuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9Ib21lLmpzIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vLi9zcmMvTG9naW4uanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9NZW51LmpzIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vLi9zcmMvUHJlZGljdGlvbkZvcm0uanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9Qcml2YXRlUm91dGUuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9TaWduVXAuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9VcGRhdGVQcm9maWxlLmpzIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vLi9zcmMvY29udGV4dHMvQXV0aENvbnRleHQuanMiLCJ3ZWJwYWNrOi8vZW12ZWlra2F1cy9jbGlwMi1TZXR0aW5nLVVwLVRvb2xjaGFpbi8uL3NyYy9maXJlLmpzIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vZXh0ZXJuYWwgXCJmaXJlYmFzZS9hcHBcIiIsIndlYnBhY2s6Ly9lbXZlaWtrYXVzL2NsaXAyLVNldHRpbmctVXAtVG9vbGNoYWluL2V4dGVybmFsIFwiZmlyZWJhc2UvYXV0aFwiIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vZXh0ZXJuYWwgXCJmaXJlYmFzZS9kYXRhYmFzZVwiIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vZXh0ZXJuYWwgXCJyZWFjdC1ib290c3RyYXBcIiIsIndlYnBhY2s6Ly9lbXZlaWtrYXVzL2NsaXAyLVNldHRpbmctVXAtVG9vbGNoYWluL2V4dGVybmFsIFwicmVhY3Qtcm91dGVyLWRvbVwiIiwid2VicGFjazovL2VtdmVpa2thdXMvY2xpcDItU2V0dGluZy1VcC1Ub29sY2hhaW4vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiJdLCJuYW1lcyI6WyJpbmRleCIsIkFwcCIsIkhvbWUiLCJVcGRhdGVQcm9maWxlIiwiRGFzaGJvYXJkIiwiU2lnblVwIiwiTG9naW4iLCJGb3Jnb3RQYXNzd29yZCIsIkdhbWVEZXRhaWwiLCJlcnJvciIsInNldEVycm9yIiwidXNlU3RhdGUiLCJjdXJyZW50VXNlciIsImxvZ291dCIsInVzZUF1dGgiLCJlbWFpbCIsImRpc3BsYXlOYW1lIiwiZW1haWxSZWYiLCJ1c2VSZWYiLCJyZXNldFBhc3N3b3JkIiwibWVzc2FnZSIsInNldE1lc3NhZ2UiLCJsb2FkaW5nIiwic2V0TG9hZGluZyIsImhhbmRsZVN1Ym1pdCIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImN1cnJlbnQiLCJ2YWx1ZSIsImNvbG9yIiwiZ2FtZXMiLCJzZXRHYW1lcyIsImdldE1hdGNoRGF0YSIsImRiIiwib3JkZXJCeUNoaWxkIiwib24iLCJzbmFwc2hvdCIsInZhbCIsInVzZUVmZmVjdCIsImRpc3BsYXkiLCJPYmplY3QiLCJrZXlzIiwibWFwIiwiaXRlbSIsImkiLCJob21lVGVhbSIsImF3YXlUZWFtIiwiRGF0ZSIsImRhdGVUaW1lIiwidG9Mb2NhbGVTdHJpbmciLCJmaW5pc2hlZCIsIkhlYWRlciIsInBhc3N3b3JkUmVmIiwibG9naW4iLCJoaXN0b3J5IiwidXNlSGlzdG9yeSIsInB1c2giLCJNZW51IiwiaGFuZGxlTG9nb3V0IiwiUHJlZGljdGlvbkZvcm0iLCJwcmVkaWN0aW9uU2F2ZUhhbmRsZXIiLCJldmVudCIsImNvbnNvbGUiLCJsb2ciLCJQcml2YXRlUm91dGUiLCJjb21wb25lbnQiLCJDb21wb25lbnQiLCJyZXN0IiwicHJvcHMiLCJwYXNzd29yZENvbmZpcm1SZWYiLCJzaWdudXAiLCJkaXNwbGF5TmFtZVJlZiIsInVwZGF0ZVBhc3N3b3JkIiwidXBkYXRlRW1haWwiLCJ1cGRhdGVEaXNwbGF5TmFtZSIsInByb21pc2VzIiwiUHJvbWlzZSIsImFsbCIsInRoZW4iLCJjYXRjaCIsImZpbmFsbHkiLCJBdXRoQ29udGV4dCIsIlJlYWN0IiwidXNlQ29udGV4dCIsIkF1dGhQcm92aWRlciIsImNoaWxkcmVuIiwic2V0Q3VycmVudFVzZXIiLCJwYXNzd29yZCIsImF1dGgiLCJ1cGRhdGVQcm9maWxlIiwidW5zdWJzY3JpYmUiLCJ1c2VyIiwiZmlyZSIsImZpcmViYXNlQ29uZmlnIiwiYXBpS2V5IiwiYXV0aERvbWFpbiIsImRhdGFiYXNlVVJMIiwicHJvamVjdElkIiwic3RvcmFnZUJ1Y2tldCIsIm1lc3NhZ2luZ1NlbmRlcklkIiwiYXBwSWQiLCJtZWFzdXJlbWVudElkIiwiZmlyZWJhc2UiLCJkYXRhYmFzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTUEsS0FBSyxHQUFFLE1BQUs7QUFDZCxzQkFBTyw4REFBQyxnREFBRDtBQUFLLFlBQVEsRUFBQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFBUDtBQUNILENBRkQ7O0FBSUEsK0RBQWVBLEtBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1DLEdBQUcsR0FBQyxNQUFJO0FBQ1Ysc0JBRVk7QUFBSyxhQUFTLEVBQUMsT0FBZjtBQUFBLDJCQUNLLDhEQUFDLDJEQUFEO0FBQUEsNkJBQ0csOERBQUMsK0RBQUQ7QUFBQSwrQkFDSSw4REFBQyxvREFBRDtBQUFBLGtDQUNJLDhEQUFDLG1EQUFEO0FBQU8saUJBQUssTUFBWjtBQUFhLGdCQUFJLEVBQUMsR0FBbEI7QUFBc0IscUJBQVMsRUFBRUMsMkNBQUlBO0FBQXJDO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREosZUFFSSw4REFBQyxrREFBRDtBQUFjLGdCQUFJLEVBQUMsaUJBQW5CO0FBQXFDLHFCQUFTLEVBQUVDLG1EQUFhQTtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUZKLGVBR0ksOERBQUMsa0RBQUQ7QUFBYyxnQkFBSSxFQUFDLFlBQW5CO0FBQWdDLHFCQUFTLEVBQUVDLCtDQUFTQTtBQUFwRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUhKLGVBSUksOERBQUMsbURBQUQ7QUFBTyxnQkFBSSxFQUFDLFNBQVo7QUFBc0IscUJBQVMsRUFBRUMsNENBQU1BO0FBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBSkosZUFLSSw4REFBQyxtREFBRDtBQUFPLGdCQUFJLEVBQUMsUUFBWjtBQUFxQixxQkFBUyxFQUFFQywyQ0FBS0E7QUFBckM7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFMSixlQU1JLDhEQUFDLG1EQUFEO0FBQU8sZ0JBQUksRUFBQyxrQkFBWjtBQUErQixxQkFBUyxFQUFFQyxvREFBY0E7QUFBeEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFOSixlQU9JLDhEQUFDLG1EQUFEO0FBQU8sZ0JBQUksRUFBQyxVQUFaO0FBQXVCLHFCQUFTLEVBQUVDLGlEQUFVQTtBQUE1QztBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREw7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZaO0FBbUJILENBcEJEOztBQXFCQSwrREFBZVAsR0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFZSxTQUFTRyxTQUFULEdBQW9CO0FBQy9CLFFBQU07QUFBQSxPQUFDSyxLQUFEO0FBQUEsT0FBUUM7QUFBUixNQUFvQkMsK0NBQVEsQ0FBQyxFQUFELENBQWxDO0FBQ0EsUUFBTTtBQUFDQyxlQUFEO0FBQWNDO0FBQWQsTUFBd0JDLDhEQUFPLEVBQXJDO0FBQ0Esc0JBQ0k7QUFBQSw0QkFDQSw4REFBQyw0Q0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREEsZUFFQSw4REFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkEsZUFHQSw4REFBQyxzREFBRDtBQUFBLDhCQUNJLDhEQUFDLGlEQUFEO0FBQUEsK0JBQ0ksOERBQUMsc0RBQUQ7QUFBQSxrQ0FDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixFQUVLTCxLQUFLLGlCQUFJLDhEQUFDLEtBQUQ7QUFBQSxzQkFBUUE7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZkLGVBR0k7QUFBQSxvQ0FBSztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFBTCxFQUE4QkcsV0FBVyxDQUFDRyxLQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBSEosZUFJSTtBQUFBLG9DQUFLO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUFMLEVBQWlDSCxXQUFXLENBQUNJLFdBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFKSixlQUtJLDhEQUFDLG1EQUFEO0FBQUEsbUNBQ0ksOERBQUMsa0RBQUQ7QUFBTSxnQkFBRSxFQUFDLGlCQUFUO0FBQTJCLHVCQUFTLEVBQUMsV0FBckM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUxKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQVlJO0FBQUssaUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FaSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFIQTtBQUFBLGtCQURKO0FBcUJILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNULGNBQVQsR0FBeUI7QUFDdEMsUUFBTVUsUUFBUSxHQUFHQyw2Q0FBTSxFQUF2QjtBQUNBLFFBQU07QUFBQ0M7QUFBRCxNQUFrQkwsOERBQU8sRUFBL0I7QUFDQSxRQUFNO0FBQUEsT0FBQ0wsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0JDLCtDQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDUyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QlYsK0NBQVEsQ0FBQyxFQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUNXLE9BQUQ7QUFBQSxPQUFVQztBQUFWLE1BQXdCWiwrQ0FBUSxDQUFDLEtBQUQsQ0FBdEM7O0FBRUEsaUJBQWVhLFlBQWYsQ0FBNEJDLENBQTVCLEVBQThCO0FBQzVCQSxLQUFDLENBQUNDLGNBQUY7O0FBQ0EsUUFBSTtBQUNKTCxnQkFBVSxDQUFDLEVBQUQsQ0FBVjtBQUNFWCxjQUFRLENBQUMsRUFBRCxDQUFSO0FBQ0FhLGdCQUFVLENBQUMsSUFBRCxDQUFWO0FBQ0EsWUFBTUosYUFBYSxDQUFDRixRQUFRLENBQUNVLE9BQVQsQ0FBaUJDLEtBQWxCLENBQW5CO0FBQ0FQLGdCQUFVLENBQUMsa0JBQUQsQ0FBVjtBQUNELEtBTkQsQ0FNRSxNQUFNO0FBQ05YLGNBQVEsQ0FBQywwQkFBRCxDQUFSO0FBQ0Q7O0FBQ0RhLGNBQVUsQ0FBQyxLQUFELENBQVY7QUFDRDs7QUFDRCxzQkFDRTtBQUFBLDRCQUNBLDhEQUFDLGlEQUFEO0FBQUEsNkJBQ0UsOERBQUMsc0RBQUQ7QUFBQSxnQ0FDRTtBQUFJLG1CQUFTLEVBQUMsa0JBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsRUFFR2QsS0FBSyxpQkFBSSw4REFBQyxrREFBRDtBQUFPLGVBQUssRUFBRTtBQUFDb0IsaUJBQUssRUFBRTtBQUFSLFdBQWQ7QUFBQSxvQkFBK0JwQjtBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZaLGVBR0UsOERBQUMsaURBQUQ7QUFBTSxrQkFBUSxFQUFFZSxZQUFoQjtBQUFBLGtDQUNFLDhEQUFDLHVEQUFEO0FBQVksY0FBRSxFQUFDLE9BQWY7QUFBQSxvQ0FDRSw4REFBQyx1REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixlQUVFLDhEQUFDLHlEQUFEO0FBQWMsa0JBQUksRUFBQyxPQUFuQjtBQUEyQixpQkFBRyxFQUFFUCxRQUFoQztBQUEwQyxzQkFBUTtBQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUtFLDhEQUFDLG1EQUFEO0FBQVEsb0JBQVEsRUFBRUssT0FBbEI7QUFBMkIscUJBQVMsRUFBQyxPQUFyQztBQUE2QyxnQkFBSSxFQUFDLFFBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFIRixlQVVFO0FBQUEsaUNBQ0ksOERBQUMsa0RBQUQ7QUFBTSxjQUFFLEVBQUMsUUFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBVkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURBLGVBaUJBO0FBQUssZUFBUyxFQUFDLHdCQUFmO0FBQUEsd0RBQ3dCLDhEQUFDLGtEQUFEO0FBQU0sVUFBRSxFQUFDLFNBQVQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FEeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBakJBO0FBQUEsa0JBREY7QUF1QkQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsREQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTWQsVUFBVSxHQUFHLE1BQUs7QUFFcEIsUUFBTTtBQUFDSSxlQUFEO0FBQWNDO0FBQWQsTUFBd0JDLDhEQUFPLEVBQXJDO0FBQ0EsUUFBTTtBQUFBLE9BQUNnQixLQUFEO0FBQUEsT0FBUUM7QUFBUixNQUFvQnBCLCtDQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDVyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QlosK0NBQVEsQ0FBQyxLQUFELENBQXRDOztBQUVBLFFBQU1xQixZQUFZLEdBQUcsTUFBTTtBQUN2QkMsZ0RBQUEsQ0FBTyxVQUFQLEVBQW1CQyxZQUFuQixDQUFnQyxVQUFoQyxFQUE0Q0MsRUFBNUMsQ0FBK0MsT0FBL0MsRUFBeURDLFFBQUQsSUFBYztBQUNsRUwsY0FBUSxDQUFDSyxRQUFRLENBQUNDLEdBQVQsRUFBRCxDQUFSLENBRGtFLENBRWxFOztBQUNBZCxnQkFBVSxDQUFDLElBQUQsQ0FBVjtBQUNILEtBSkQ7QUFLQSxXQUFPLE1BQU1VLGdEQUFBLEVBQWI7QUFDSCxHQVBEOztBQVNBSyxrREFBUyxDQUFDLE1BQUk7QUFDTk4sZ0JBQVk7QUFDbkIsR0FGUSxFQUVQLEVBRk8sQ0FBVDs7QUFHQSxNQUFHLENBQUNWLE9BQUosRUFBWTtBQUNSLHdCQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURKO0FBR0gsR0FKRCxNQUlLO0FBQ0Qsd0JBQ0k7QUFBQSw4QkFDQSw4REFBQywrQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURBLGVBRUEsOERBQUMsNkNBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGQSxlQUdBLDhEQUFDLHNEQUFEO0FBQVcsYUFBSyxFQUFFO0FBQUNpQixpQkFBTyxFQUFFO0FBQVYsU0FBbEI7QUFBQSxrQkFDQ0MsTUFBTSxDQUFDQyxJQUFQLENBQVlYLEtBQVosRUFBbUJZLEdBQW5CLENBQXVCLENBQUNDLElBQUQsRUFBT0MsQ0FBUCxrQkFDcEI7QUFBQSxpQ0FDSTtBQUFLLHFCQUFTLEVBQUMsV0FBZjtBQUFBLG9DQUNJO0FBQUksdUJBQVMsRUFBQyxZQUFkO0FBQUEscUNBQ0E7QUFBQSx3Q0FDSTtBQUFLLDJCQUFTLEVBQUMsTUFBZjtBQUFzQixxQkFBRyxFQUFDO0FBQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBREosT0FDb0NkLEtBQUssQ0FBQ2EsSUFBRCxDQUFMLENBQVlFLFFBRGhELFNBQzZEZixLQUFLLENBQUNhLElBQUQsQ0FBTCxDQUFZRyxRQUR6RSxvQkFDbUY7QUFBSywyQkFBUyxFQUFDLE1BQWY7QUFBc0IscUJBQUcsRUFBQztBQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBLDZCQURuRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQURKLGVBTUo7QUFBQSx3QkFBSSxJQUFJQyxJQUFKLENBQVNqQixLQUFLLENBQUNhLElBQUQsQ0FBTCxDQUFZSyxRQUFaLEdBQXVCLElBQWhDLEVBQXNDQyxjQUF0QztBQUFKO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBTkksRUFPUCxDQUFDbkIsS0FBSyxDQUFDYSxJQUFELENBQUwsQ0FBWU8sUUFBYixJQUF5QnRDLFdBQXpCLGlCQUNELDhEQUFDLHVEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBUlE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREosV0FBVWdDLENBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFESDtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBSEE7QUFBQSxvQkFESjtBQXNCSDtBQUNKLENBOUNEOztBQWlEQSwrREFBZXBDLFVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekRBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNMkMsTUFBTSxHQUFHLE1BQU07QUFFakIsUUFBTTtBQUFDdkMsZUFBRDtBQUFjQztBQUFkLE1BQXdCQyw4REFBTyxFQUFyQztBQUNBLHNCQUNJO0FBQUssYUFBUyxFQUFDLDJCQUFmO0FBQUEsMkJBQ0k7QUFBSyxlQUFTLEVBQUMsS0FBZjtBQUFBLDZCQUNJO0FBQUssaUJBQVMsRUFBQyxnQ0FBZjtBQUFBLCtCQUNJO0FBQUEsa0NBQ0k7QUFBQSxtQ0FDSTtBQUFLLGlCQUFHLEVBQUM7QUFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFESixFQUlLRixXQUFXLGlCQUFJO0FBQUEseUNBQW1CQSxXQUFXLENBQUNJLFdBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFKcEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREo7QUFnQkgsQ0FuQkQ7O0FBb0JBLCtEQUFlbUMsTUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hCQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTW5ELEtBQUssR0FBRyxNQUFNO0FBQ2hCLHNCQUNJO0FBQUEsNEJBQ0ksOERBQUMsNENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFESixlQUVJLDhEQUFDLDBDQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRkosZUFHSTtBQUFLLGVBQVMsRUFBQyxXQUFmO0FBQUEsNkJBQ0k7QUFBSyxpQkFBUyxFQUFDLEtBQWY7QUFBQSwrQkFDSTtBQUFJLG1CQUFTLEVBQUMsRUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKO0FBY0gsQ0FmRDs7QUFpQkEsK0RBQWVBLEtBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1NLEtBQUssR0FBRyxNQUFNO0FBQ2xCLFFBQU1XLFFBQVEsR0FBR0MsNkNBQU0sRUFBdkI7QUFDQSxRQUFNa0MsV0FBVyxHQUFHbEMsNkNBQU0sRUFBMUI7QUFDQSxRQUFNO0FBQUEsT0FBQ1QsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0JDLCtDQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDVyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QlosK0NBQVEsQ0FBQyxLQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFDMEM7QUFBRCxNQUFVdkMsOERBQU8sRUFBdkI7QUFDQSxRQUFNd0MsT0FBTyxHQUFHQyw0REFBVSxFQUExQjs7QUFFQSxpQkFBZS9CLFlBQWYsQ0FBNEJDLENBQTVCLEVBQThCO0FBQzVCQSxLQUFDLENBQUNDLGNBQUY7O0FBQ0EsUUFBSTtBQUNGaEIsY0FBUSxDQUFDLEVBQUQsQ0FBUjtBQUNBYSxnQkFBVSxDQUFDLElBQUQsQ0FBVjtBQUNBLFlBQU04QixLQUFLLENBQUNwQyxRQUFRLENBQUNVLE9BQVQsQ0FBaUJDLEtBQWxCLEVBQXlCd0IsV0FBVyxDQUFDekIsT0FBWixDQUFvQkMsS0FBN0MsQ0FBWDtBQUNBMEIsYUFBTyxDQUFDRSxJQUFSLENBQWEsR0FBYjtBQUNELEtBTEQsQ0FLRSxNQUFNO0FBQ045QyxjQUFRLENBQUMsa0JBQUQsQ0FBUjtBQUNEOztBQUNEYSxjQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0Q7O0FBQ0Qsc0JBQ0U7QUFBQSw0QkFDQSw4REFBQyw0Q0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURBLGVBRUEsOERBQUMsMENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGQSxlQUdBLDhEQUFDLHNEQUFEO0FBQUEsOEJBQ0EsOERBQUMsaURBQUQ7QUFBQSwrQkFDRSw4REFBQyxzREFBRDtBQUFBLGtDQUNFO0FBQUkscUJBQVMsRUFBQyxrQkFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFERixFQUVHZCxLQUFLLGlCQUFJLDhEQUFDLGtEQUFEO0FBQU8saUJBQUssRUFBRTtBQUFDb0IsbUJBQUssRUFBRTtBQUFSLGFBQWQ7QUFBQSxzQkFBK0JwQjtBQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUZaLGVBR0UsOERBQUMsaURBQUQ7QUFBTSxvQkFBUSxFQUFFZSxZQUFoQjtBQUFBLG9DQUNFLDhEQUFDLHVEQUFEO0FBQVksZ0JBQUUsRUFBQyxPQUFmO0FBQUEsc0NBQ0UsOERBQUMsdURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBREYsZUFFRSw4REFBQyx5REFBRDtBQUFjLG9CQUFJLEVBQUMsT0FBbkI7QUFBMkIsbUJBQUcsRUFBRVAsUUFBaEM7QUFBMEMsd0JBQVE7QUFBbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBREYsZUFLRSw4REFBQyx1REFBRDtBQUFZLGdCQUFFLEVBQUMsVUFBZjtBQUFBLHNDQUNFLDhEQUFDLHVEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQURGLGVBRUUsOERBQUMseURBQUQ7QUFBYyxvQkFBSSxFQUFDLFVBQW5CO0FBQThCLG1CQUFHLEVBQUVtQyxXQUFuQztBQUFnRCx3QkFBUTtBQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFMRixlQVNFLDhEQUFDLG1EQUFEO0FBQVEsc0JBQVEsRUFBRTlCLE9BQWxCO0FBQTJCLHVCQUFTLEVBQUMsT0FBckM7QUFBNkMsa0JBQUksRUFBQyxRQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFURjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBSEYsZUFjRTtBQUFBLG1DQUNJLDhEQUFDLGtEQUFEO0FBQU0sZ0JBQUUsRUFBQyxrQkFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBZEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFEQSxlQXFCQTtBQUFLLGlCQUFTLEVBQUMsd0JBQWY7QUFBQSwwREFDd0IsOERBQUMsa0RBQUQ7QUFBTSxZQUFFLEVBQUMsU0FBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSEE7QUFBQSxrQkFERjtBQStCRCxDQW5ERDs7QUFxREEsK0RBQWVoQixLQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVEQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTW1ELElBQUksR0FBRyxNQUFNO0FBRWYsUUFBTTtBQUFBLE9BQUNoRCxLQUFEO0FBQUEsT0FBUUM7QUFBUixNQUFvQkMsK0NBQVEsQ0FBQyxFQUFELENBQWxDO0FBQ0EsUUFBTTtBQUFDQyxlQUFEO0FBQWNDO0FBQWQsTUFBd0JDLDhEQUFPLEVBQXJDO0FBQ0EsUUFBTXdDLE9BQU8sR0FBR0MsNERBQVUsRUFBMUI7O0FBQ0EsaUJBQWVHLFlBQWYsR0FBNkI7QUFDekJoRCxZQUFRLENBQUMsRUFBRCxDQUFSOztBQUNBLFFBQUc7QUFDQyxZQUFNRyxNQUFNLEVBQVo7QUFDQXlDLGFBQU8sQ0FBQ0UsSUFBUixDQUFhLFFBQWI7QUFDSCxLQUhELENBR0MsTUFBSztBQUNGOUMsY0FBUSxDQUFDLGtCQUFELENBQVI7QUFDSDtBQUNKOztBQUNELHNCQUNJO0FBQUssU0FBSyxFQUFFO0FBQUM2QixhQUFPLEVBQUU7QUFBVixLQUFaO0FBQUEsMkJBQ0k7QUFBQSw2QkFDSTtBQUFBLGdDQUNJO0FBQUEsaUNBQ0ksOERBQUMsa0RBQUQ7QUFBTSxjQUFFLEVBQUMsR0FBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREosZUFNSTtBQUFBLGlDQUNJLDhEQUFDLGtEQUFEO0FBQU0sY0FBRSxFQUFDLFVBQVQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQU5KLEVBV0szQixXQUFXLGlCQUNaO0FBQUEsaUNBQ0ksOERBQUMsa0RBQUQ7QUFBTSxjQUFFLEVBQUMsWUFBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBWkosZUFrQkk7QUFBQSxxQkFDSyxDQUFDQSxXQUFELGlCQUNELDhEQUFDLGtEQUFEO0FBQU0sY0FBRSxFQUFDLFFBQVQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRkosRUFNS0EsV0FBVyxpQkFDWiw4REFBQyxrREFBRDtBQUFNLGNBQUUsRUFBQyxHQUFUO0FBQWEsbUJBQU8sRUFBRThDLFlBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQVBKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFsQko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFESjtBQXFDSCxDQW5ERDs7QUFxREEsK0RBQWVELElBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekRBOztBQUNBLE1BQU1FLGNBQWMsR0FBRyxNQUFNO0FBRXpCLFdBQVNDLHFCQUFULENBQStCQyxLQUEvQixFQUFzQztBQUNsQ0EsU0FBSyxDQUFDbkMsY0FBTjtBQUNBb0MsV0FBTyxDQUFDQyxHQUFSLENBQVksV0FBWjtBQUNIOztBQUNHLHNCQUNJO0FBQUEsMkJBQ0k7QUFBTSxjQUFRLEVBQUVILHFCQUFoQjtBQUFBLDhCQUNJO0FBQU8saUJBQVMsRUFBQyxZQUFqQjtBQUE4QixZQUFJLEVBQUMsUUFBbkM7QUFBNEMsZ0JBQVE7QUFBcEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFESixlQUVJO0FBQU8saUJBQVMsRUFBQyxZQUFqQjtBQUE4QixZQUFJLEVBQUMsUUFBbkM7QUFBNEMsZ0JBQVE7QUFBcEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGSixlQUdJO0FBQVEsWUFBSSxFQUFDLFFBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBSEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKO0FBVVAsQ0FoQkQ7O0FBa0JBLCtEQUFlRCxjQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNlLFNBQVNLLFlBQVQsT0FBc0Q7QUFBQSxNQUFoQztBQUFDQyxhQUFTLEVBQUVDO0FBQVosR0FBZ0M7QUFBQSxNQUFOQyxJQUFNOztBQUNqRSxRQUFNO0FBQUN2RDtBQUFELE1BQWdCRSw4REFBTyxFQUE3QjtBQUNBLHNCQUNJLDhEQUFDLG1EQUFELGtDQUNRcUQsSUFEUjtBQUVJLFVBQU0sRUFBRUMsS0FBSyxJQUFJO0FBQ2IsYUFBT3hELFdBQVcsZ0JBQUcsOERBQUMsU0FBRCxvQkFBZXdELEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUFILGdCQUE4Qiw4REFBQyxzREFBRDtBQUFVLFVBQUUsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBaEQ7QUFDSDtBQUpMO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQVFILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDYkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU0vRCxNQUFNLEdBQUcsTUFBTTtBQUNuQixRQUFNWSxRQUFRLEdBQUdDLDZDQUFNLEVBQXZCO0FBQ0EsUUFBTWtDLFdBQVcsR0FBR2xDLDZDQUFNLEVBQTFCO0FBQ0EsUUFBTW1ELGtCQUFrQixHQUFHbkQsNkNBQU0sRUFBakM7QUFDQSxRQUFNO0FBQUEsT0FBQ1QsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0JDLCtDQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDVyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QlosK0NBQVEsQ0FBQyxLQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFDMkQ7QUFBRCxNQUFXeEQsOERBQU8sRUFBeEI7O0FBRUEsaUJBQWVVLFlBQWYsQ0FBNEJDLENBQTVCLEVBQThCO0FBQzVCQSxLQUFDLENBQUNDLGNBQUY7O0FBQ0EsUUFBRzBCLFdBQVcsQ0FBQ3pCLE9BQVosQ0FBb0JDLEtBQXBCLEtBQThCeUMsa0JBQWtCLENBQUMxQyxPQUFuQixDQUEyQkMsS0FBNUQsRUFBa0U7QUFDaEUsYUFBT2xCLFFBQVEsQ0FBQyx3QkFBRCxDQUFmO0FBQ0Q7O0FBQ0QsUUFBSTtBQUNGQSxjQUFRLENBQUMsRUFBRCxDQUFSO0FBQ0FhLGdCQUFVLENBQUMsSUFBRCxDQUFWO0FBQ0EsWUFBTStDLE1BQU0sQ0FBQ3JELFFBQVEsQ0FBQ1UsT0FBVCxDQUFpQkMsS0FBbEIsRUFBeUJ3QixXQUFXLENBQUN6QixPQUFaLENBQW9CQyxLQUE3QyxDQUFaO0FBQ0QsS0FKRCxDQUlFLE1BQU07QUFDTmxCLGNBQVEsQ0FBQyxtQkFBRCxDQUFSO0FBQ0Q7O0FBQ0RhLGNBQVUsQ0FBQyxLQUFELENBQVY7QUFDRDs7QUFDRCxzQkFDRTtBQUFBLDRCQUNBLDhEQUFDLDRDQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREEsZUFFQSw4REFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZBLGVBR0EsOERBQUMsc0RBQUQ7QUFBQSw4QkFDQSw4REFBQyxpREFBRDtBQUFBLCtCQUNFLDhEQUFDLHNEQUFEO0FBQUEsa0NBQ0U7QUFBSSxxQkFBUyxFQUFDLGtCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQURGLEVBRUdkLEtBQUssaUJBQUksOERBQUMsa0RBQUQ7QUFBTyxpQkFBSyxFQUFFO0FBQUNvQixtQkFBSyxFQUFFO0FBQVIsYUFBZDtBQUFBLHNCQUErQnBCO0FBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRlosZUFHRSw4REFBQyxpREFBRDtBQUFNLG9CQUFRLEVBQUVlLFlBQWhCO0FBQUEsb0NBQ0UsOERBQUMsdURBQUQ7QUFBWSxnQkFBRSxFQUFDLE9BQWY7QUFBQSxzQ0FDRSw4REFBQyx1REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkFERixlQUVFLDhEQUFDLHlEQUFEO0FBQWMsb0JBQUksRUFBQyxPQUFuQjtBQUEyQixtQkFBRyxFQUFFUCxRQUFoQztBQUEwQyx3QkFBUTtBQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFERixlQUtFLDhEQUFDLHVEQUFEO0FBQVksZ0JBQUUsRUFBQyxVQUFmO0FBQUEsc0NBQ0UsOERBQUMsdURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBREYsZUFFRSw4REFBQyx5REFBRDtBQUFjLG9CQUFJLEVBQUMsVUFBbkI7QUFBOEIsbUJBQUcsRUFBRW1DLFdBQW5DO0FBQWdELHdCQUFRO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQUxGLGVBU0UsOERBQUMsdURBQUQ7QUFBWSxnQkFBRSxFQUFDLGtCQUFmO0FBQUEsc0NBQ0UsOERBQUMsdURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBREYsZUFFRSw4REFBQyx5REFBRDtBQUFjLG9CQUFJLEVBQUMsVUFBbkI7QUFBOEIsbUJBQUcsRUFBRWlCLGtCQUFuQztBQUF1RCx3QkFBUTtBQUEvRDtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFURixlQWFFLDhEQUFDLG1EQUFEO0FBQVEsc0JBQVEsRUFBRS9DLE9BQWxCO0FBQTJCLHVCQUFTLEVBQUMsT0FBckM7QUFBNkMsa0JBQUksRUFBQyxRQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFiRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBSEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFEQSxlQXVCQTtBQUFLLGlCQUFTLEVBQUMsd0JBQWY7QUFBQSw2REFDMkIsOERBQUMsa0RBQUQ7QUFBTSxZQUFFLEVBQUMsT0FBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXZCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSEE7QUFBQSxrQkFERjtBQWlDRCxDQXZERDs7QUF5REEsK0RBQWVqQixNQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNRixhQUFhLEdBQUcsTUFBTTtBQUMxQixRQUFNYyxRQUFRLEdBQUdDLDZDQUFNLEVBQXZCO0FBQ0EsUUFBTWtDLFdBQVcsR0FBR2xDLDZDQUFNLEVBQTFCO0FBQ0EsUUFBTXFELGNBQWMsR0FBR3JELDZDQUFNLEVBQTdCO0FBQ0EsUUFBTW1ELGtCQUFrQixHQUFHbkQsNkNBQU0sRUFBakM7QUFDQSxRQUFNO0FBQUEsT0FBQ1QsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0JDLCtDQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDVyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QlosK0NBQVEsQ0FBQyxLQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFDQyxlQUFEO0FBQWM0RCxrQkFBZDtBQUE4QkMsZUFBOUI7QUFBMkNDO0FBQTNDLE1BQWdFNUQsOERBQU8sRUFBN0U7QUFDQSxRQUFNd0MsT0FBTyxHQUFHQyw0REFBVSxFQUExQjs7QUFFQSxXQUFTL0IsWUFBVCxDQUFzQkMsQ0FBdEIsRUFBeUI7QUFDdkJBLEtBQUMsQ0FBQ0MsY0FBRjs7QUFDQSxRQUFHMEIsV0FBVyxDQUFDekIsT0FBWixDQUFvQkMsS0FBcEIsS0FBOEJ5QyxrQkFBa0IsQ0FBQzFDLE9BQW5CLENBQTJCQyxLQUE1RCxFQUFrRTtBQUNoRSxhQUFPbEIsUUFBUSxDQUFDLHdCQUFELENBQWY7QUFDRDs7QUFFSCxVQUFNaUUsUUFBUSxHQUFHLEVBQWpCO0FBQ0VwRCxjQUFVLENBQUMsSUFBRCxDQUFWO0FBQ0FiLFlBQVEsQ0FBQyxFQUFELENBQVI7O0FBQ0YsUUFBR08sUUFBUSxDQUFDVSxPQUFULENBQWlCQyxLQUFqQixLQUEyQmhCLFdBQVcsQ0FBQ0csS0FBMUMsRUFBZ0Q7QUFDNUM0RCxjQUFRLENBQUNuQixJQUFULENBQWNpQixXQUFXLENBQUN4RCxRQUFRLENBQUNVLE9BQVQsQ0FBaUJDLEtBQWxCLENBQXpCO0FBQ0g7O0FBQ0QsUUFBR3dCLFdBQVcsQ0FBQ3pCLE9BQVosQ0FBb0JDLEtBQXZCLEVBQTZCO0FBQzNCK0MsY0FBUSxDQUFDbkIsSUFBVCxDQUFjZ0IsY0FBYyxDQUFDcEIsV0FBVyxDQUFDekIsT0FBWixDQUFvQkMsS0FBckIsQ0FBNUI7QUFDRDs7QUFDRCxRQUFHMkMsY0FBYyxDQUFDNUMsT0FBZixDQUF1QkMsS0FBdkIsS0FBaUNoQixXQUFXLENBQUNJLFdBQWhELEVBQTREO0FBQzFEMkQsY0FBUSxDQUFDbkIsSUFBVCxDQUFja0IsaUJBQWlCLENBQUNILGNBQWMsQ0FBQzVDLE9BQWYsQ0FBdUJDLEtBQXhCLENBQS9CO0FBQ0Q7O0FBQ0NnRCxXQUFPLENBQUNDLEdBQVIsQ0FBWUYsUUFBWixFQUNDRyxJQURELENBQ00sTUFBSTtBQUNOeEIsYUFBTyxDQUFDRSxJQUFSLENBQWEsR0FBYjtBQUNILEtBSEQsRUFHR3VCLEtBSEgsQ0FHUyxNQUFJO0FBQ1RyRSxjQUFRLENBQUMsMEJBQUQsQ0FBUjtBQUNILEtBTEQsRUFLR3NFLE9BTEgsQ0FLVyxNQUFJO0FBQ1h6RCxnQkFBVSxDQUFDLEtBQUQsQ0FBVjtBQUNILEtBUEQ7QUFRRDs7QUFDRCxzQkFDRTtBQUFBLDRCQUNBLDhEQUFDLDRDQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREEsZUFFQSw4REFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZBLGVBR0EsOERBQUMsc0RBQUQ7QUFBQSw4QkFDQSw4REFBQyxpREFBRDtBQUFBLCtCQUNFLDhEQUFDLHNEQUFEO0FBQUEsa0NBQ0U7QUFBSSxxQkFBUyxFQUFDLGtCQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQURGLEVBRUdkLEtBQUssaUJBQUksOERBQUMsa0RBQUQ7QUFBTyxpQkFBSyxFQUFFO0FBQUNvQixtQkFBSyxFQUFFO0FBQVIsYUFBZDtBQUFBLHNCQUErQnBCO0FBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRlosZUFHRSw4REFBQyxpREFBRDtBQUFNLG9CQUFRLEVBQUVlLFlBQWhCO0FBQUEsb0NBQ0UsOERBQUMsdURBQUQ7QUFBWSxnQkFBRSxFQUFDLE9BQWY7QUFBQSxzQ0FDRSw4REFBQyx1REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkFERixlQUVFLDhEQUFDLHlEQUFEO0FBQWMsb0JBQUksRUFBQyxNQUFuQjtBQUEwQixtQkFBRyxFQUFFK0MsY0FBL0I7QUFBK0MsNEJBQVksRUFBRTNELFdBQVcsQ0FBQ0k7QUFBekU7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBREYsZUFLRSw4REFBQyx1REFBRDtBQUFZLGdCQUFFLEVBQUMsT0FBZjtBQUFBLHNDQUNFLDhEQUFDLHVEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQURGLGVBRUUsOERBQUMseURBQUQ7QUFBYyxvQkFBSSxFQUFDLE9BQW5CO0FBQTJCLG1CQUFHLEVBQUVDLFFBQWhDO0FBQTBDLDRCQUFZLEVBQUVMLFdBQVcsQ0FBQ0c7QUFBcEU7QUFBQTtBQUFBO0FBQUE7QUFBQSwyQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBTEYsZUFTRSw4REFBQyx1REFBRDtBQUFZLGdCQUFFLEVBQUMsVUFBZjtBQUFBLHNDQUNFLDhEQUFDLHVEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQURGLGVBRUUsOERBQUMseURBQUQ7QUFBYyxvQkFBSSxFQUFDLFVBQW5CO0FBQThCLG1CQUFHLEVBQUVxQyxXQUFuQztBQUFnRCwyQkFBVyxFQUFDO0FBQTVEO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQVRGLGVBYUUsOERBQUMsdURBQUQ7QUFBWSxnQkFBRSxFQUFDLGtCQUFmO0FBQUEsc0NBQ0UsOERBQUMsdURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBREYsZUFFRSw4REFBQyx5REFBRDtBQUFjLG9CQUFJLEVBQUMsVUFBbkI7QUFBOEIsbUJBQUcsRUFBRWlCLGtCQUFuQztBQUF1RCwyQkFBVyxFQUFDO0FBQW5FO0FBQUE7QUFBQTtBQUFBO0FBQUEsMkJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHlCQWJGLGVBaUJFLDhEQUFDLG1EQUFEO0FBQVEsc0JBQVEsRUFBRS9DLE9BQWxCO0FBQTJCLHVCQUFTLEVBQUMsT0FBckM7QUFBNkMsa0JBQUksRUFBQyxRQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFqQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREEsZUEwQkE7QUFBSyxpQkFBUyxFQUFDLHdCQUFmO0FBQUEsK0JBQ0UsOERBQUMsa0RBQUQ7QUFBTSxZQUFFLEVBQUMsR0FBVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBMUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFIQTtBQUFBLGtCQURGO0FBb0NELENBekVEOztBQTJFQSwrREFBZW5CLGFBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQTtBQUVBLE1BQU04RSxXQUFXLGdCQUFHQywwREFBQSxFQUFwQjtBQUVPLFNBQVNwRSxPQUFULEdBQWtCO0FBQ3JCLFNBQU9xRSxpREFBVSxDQUFDRixXQUFELENBQWpCO0FBQ0g7QUFFTSxTQUFTRyxZQUFULENBQXNCO0FBQUNDO0FBQUQsQ0FBdEIsRUFBaUM7QUFFcEMsUUFBTTtBQUFBLE9BQUN6RSxXQUFEO0FBQUEsT0FBYzBFO0FBQWQsTUFBOEIzRSwrQ0FBUSxFQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDVyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUFzQlosK0NBQVEsQ0FBQyxJQUFELENBQXBDOztBQUVBLFdBQVMyRCxNQUFULENBQWdCdkQsS0FBaEIsRUFBc0J3RSxRQUF0QixFQUErQjtBQUMzQixXQUFPQyxzRUFBQSxDQUFvQ3pFLEtBQXBDLEVBQTBDd0UsUUFBMUMsQ0FBUDtBQUNIOztBQUNELFdBQVNsQyxLQUFULENBQWV0QyxLQUFmLEVBQXFCd0UsUUFBckIsRUFBOEI7QUFDMUIsV0FBT0Msa0VBQUEsQ0FBZ0N6RSxLQUFoQyxFQUFzQ3dFLFFBQXRDLENBQVA7QUFDSDs7QUFDRCxXQUFTMUUsTUFBVCxHQUFpQjtBQUNiLFdBQU8yRSwrQ0FBQSxFQUFQO0FBQ0g7O0FBQ0QsV0FBU3JFLGFBQVQsQ0FBdUJKLEtBQXZCLEVBQTZCO0FBQ3pCLFdBQU95RSw4REFBQSxDQUE0QnpFLEtBQTVCLENBQVA7QUFDSDs7QUFDRCxXQUFTMEQsV0FBVCxDQUFxQjFELEtBQXJCLEVBQTJCO0FBQ3ZCLFdBQU9ILFdBQVcsQ0FBQzZELFdBQVosQ0FBd0IxRCxLQUF4QixDQUFQO0FBQ0g7O0FBQ0QsV0FBU3lELGNBQVQsQ0FBd0JlLFFBQXhCLEVBQWlDO0FBQzdCLFdBQU8zRSxXQUFXLENBQUM0RCxjQUFaLENBQTJCZSxRQUEzQixDQUFQO0FBQ0g7O0FBQ0QsV0FBU2IsaUJBQVQsQ0FBMkIxRCxXQUEzQixFQUF1QztBQUNuQyxXQUFPSixXQUFXLENBQUM2RSxhQUFaLENBQTBCO0FBQUN6RSxpQkFBVyxFQUFFQTtBQUFkLEtBQTFCLENBQVA7QUFDSDs7QUFDRHNCLGtEQUFTLENBQUMsTUFBSztBQUNYLFVBQU1vRCxXQUFXLEdBQUdGLDBEQUFBLENBQXdCRyxJQUFJLElBQUk7QUFDaERMLG9CQUFjLENBQUNLLElBQUQsQ0FBZDtBQUNBcEUsZ0JBQVUsQ0FBQyxLQUFELENBQVY7QUFDSCxLQUhtQixDQUFwQjtBQUlBLFdBQU9tRSxXQUFQO0FBQ0gsR0FOUSxFQU1QLEVBTk8sQ0FBVDtBQVNBLFFBQU05RCxLQUFLLEdBQUc7QUFDVmhCLGVBRFU7QUFFVnlDLFNBRlU7QUFHVmlCLFVBSFU7QUFJVnpELFVBSlU7QUFLVk0saUJBTFU7QUFNVnNELGVBTlU7QUFPVkQsa0JBUFU7QUFRVkU7QUFSVSxHQUFkO0FBV0Esc0JBQU87QUFBQSwyQkFDSCw4REFBQyxXQUFELENBQWEsUUFBYjtBQUFzQixXQUFLLEVBQUU5QyxLQUE3QjtBQUFBLGlCQUNLLENBQUNOLE9BQUQsSUFBWStELFFBRGpCLEVBRUtBLFFBRkw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREc7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUFQO0FBTUgsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdERDtBQUNBO0FBQ0E7QUFDQSxJQUFJTyxJQUFKO0FBQ0EsSUFBSUMsY0FBYyxHQUFHO0FBQ25CQyxRQUFNLEVBQUUseUNBRFc7QUFFbkJDLFlBQVUsRUFBRSxrQ0FGTztBQUduQkMsYUFBVyxFQUFFLHlFQUhNO0FBSW5CQyxXQUFTLEVBQUUsa0JBSlE7QUFLbkJDLGVBQWEsRUFBRSw4QkFMSTtBQU1uQkMsbUJBQWlCLEVBQUUsZUFOQTtBQU9uQkMsT0FBSyxFQUFFLDRDQVBZO0FBUW5CQyxlQUFhLEVBQUU7QUFSSSxDQUFyQjs7QUFVRSxJQUFJLENBQUNDLGlFQUFMLEVBQTJCO0FBQ3pCVixNQUFJLEdBQUdVLGlFQUFBLENBQXVCVCxjQUF2QixDQUFQO0FBQ0YsQ0FGQSxNQUVPO0FBQ0xELE1BQUksR0FBR1UsdURBQUEsRUFBUCxDQURLLENBQ2tCO0FBQ3pCLEMsQ0FDRjtBQUNBO0FBQ0E7OztBQUNBLE1BQU1kLElBQUksR0FBR0ksSUFBSSxDQUFDSixJQUFMLEVBQWI7QUFDQSxNQUFNdkQsRUFBRSxHQUFHMkQsSUFBSSxDQUFDVyxRQUFMLEVBQVg7QUFDQTtBQUlBLCtEQUFlWCxJQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVCQSwwQzs7Ozs7Ozs7Ozs7QUNBQSwyQzs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSxtQzs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSw4Qzs7Ozs7Ozs7Ozs7QUNBQSxtRCIsImZpbGUiOiJwYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcclxuaW1wb3J0IEFwcCBmcm9tICcuLi9zcmMvQXBwLmpzJ1xyXG5pbXBvcnQgJ2Jvb3RzdHJhcC9kaXN0L2Nzcy9ib290c3RyYXAubWluLmNzcydcclxuXHJcbmNvbnN0IGluZGV4ID0oKSA9PntcclxuICAgIHJldHVybiA8QXBwIHBhZ2VOYW1lPVwiSG9tZVwiLz5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgaW5kZXgiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXHJcbmltcG9ydCBTaWduVXAgZnJvbSAnLi9TaWduVXAnXHJcbmltcG9ydCBMb2dpbiBmcm9tICcuL0xvZ2luJ1xyXG5pbXBvcnQgeyBBdXRoUHJvdmlkZXIgfSBmcm9tICcuL2NvbnRleHRzL0F1dGhDb250ZXh0J1xyXG5pbXBvcnQgeyBCcm93c2VyUm91dGVyIGFzIFJvdXRlciwgU3dpdGNoLCBSb3V0ZSB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXHJcbmltcG9ydCBEYXNoYm9hcmQgZnJvbSAnLi9EYXNoYm9hcmQnXHJcbmltcG9ydCBQcml2YXRlUm91dGUgZnJvbSAnLi9Qcml2YXRlUm91dGUnXHJcbmltcG9ydCBGb3Jnb3RQYXNzd29yZCBmcm9tICcuL0ZvcmdvdFBhc3N3b3JkJ1xyXG5pbXBvcnQgVXBkYXRlUHJvZmlsZSBmcm9tICcuL1VwZGF0ZVByb2ZpbGUnXHJcbmltcG9ydCBHYW1lRGV0YWlsIGZyb20gJy4vR2FtZURldGFpbCdcclxuaW1wb3J0IEhvbWUgZnJvbSAnLi9Ib21lJ1xyXG5cclxuY29uc3QgQXBwPSgpPT57XHJcbiAgICByZXR1cm4oXHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctMTAwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgIDxSb3V0ZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxBdXRoUHJvdmlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3dpdGNoPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb3V0ZSBleGFjdCBwYXRoPVwiL1wiIGNvbXBvbmVudD17SG9tZX0vPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxQcml2YXRlUm91dGUgcGF0aD1cIi91cGRhdGUtcHJvZmlsZVwiIGNvbXBvbmVudD17VXBkYXRlUHJvZmlsZX0vPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxQcml2YXRlUm91dGUgcGF0aD1cIi9kYXNoYm9hcmRcIiBjb21wb25lbnQ9e0Rhc2hib2FyZH0vPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb3V0ZSBwYXRoPVwiL3NpZ251cFwiIGNvbXBvbmVudD17U2lnblVwfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb3V0ZSBwYXRoPVwiL2xvZ2luXCIgY29tcG9uZW50PXtMb2dpbn0gLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Um91dGUgcGF0aD1cIi9mb3Jnb3QtcGFzc3dvcmRcIiBjb21wb25lbnQ9e0ZvcmdvdFBhc3N3b3JkfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxSb3V0ZSBwYXRoPVwiL21hdGNoZXNcIiBjb21wb25lbnQ9e0dhbWVEZXRhaWx9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N3aXRjaD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9BdXRoUHJvdmlkZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9Sb3V0ZXI+IFxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgXHJcbiAgICApXHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgQXBwIiwiaW1wb3J0IFJlYWN0LCB7dXNlU3RhdGV9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQge0NhcmQsIEJ1dHRvbiwgQ29udGFpbmVyfSBmcm9tICdyZWFjdC1ib290c3RyYXAnXHJcbmltcG9ydCB7dXNlQXV0aH0gZnJvbSAnLi9jb250ZXh0cy9BdXRoQ29udGV4dCdcclxuaW1wb3J0IHtMaW5rLCB1c2VIaXN0b3J5fSBmcm9tICdyZWFjdC1yb3V0ZXItZG9tJ1xyXG5pbXBvcnQgSGVhZGVyIGZyb20gJy4vSGVhZGVyJ1xyXG5pbXBvcnQgTWVudSBmcm9tICcuL01lbnUnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBEYXNoYm9hcmQoKXtcclxuICAgIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUoXCJcIilcclxuICAgIGNvbnN0IHtjdXJyZW50VXNlciwgbG9nb3V0fSA9IHVzZUF1dGgoKVxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8PlxyXG4gICAgICAgIDxIZWFkZXIgLz5cclxuICAgICAgICA8TWVudSAvPlxyXG4gICAgICAgIDxDb250YWluZXI+XHJcbiAgICAgICAgICAgIDxDYXJkPlxyXG4gICAgICAgICAgICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICAgICAgICAgICAgICA8aDI+UHJvZmlsZTwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAge2Vycm9yICYmIDxBbGVydD57ZXJyb3J9PC9BbGVydD59XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj48c3Ryb25nPkVtYWlsOiA8L3N0cm9uZz57Y3VycmVudFVzZXIuZW1haWx9PC9kaXY+ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj48c3Ryb25nPk5pY2tuYW1lOiA8L3N0cm9uZz57Y3VycmVudFVzZXIuZGlzcGxheU5hbWV9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvdXBkYXRlLXByb2ZpbGVcIiBjbGFzc05hbWU9XCJidG4gdy0xMDBcIj5VcGRhdGUgUHJvZmlsZTwvTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgbXQtMlwiPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L0NvbnRhaW5lcj5cclxuICAgICAgICA8Lz5cclxuICAgIClcclxufSIsImltcG9ydCBSZWFjdCwge3VzZVN0YXRlLCB1c2VSZWZ9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgSGVhZGVyIGZyb20gJy4vSGVhZGVyJ1xyXG5pbXBvcnQgTWVudSAgZnJvbSAnLi9NZW51J1xyXG5pbXBvcnQge0NhcmQsIEZvcm0sIEJ1dHRvbiwgQWxlcnR9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCdcclxuaW1wb3J0IHt1c2VBdXRofSBmcm9tICcuL2NvbnRleHRzL0F1dGhDb250ZXh0J1xyXG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEZvcmdvdFBhc3N3b3JkKCl7XHJcbiAgY29uc3QgZW1haWxSZWYgPSB1c2VSZWYoKVxyXG4gIGNvbnN0IHtyZXNldFBhc3N3b3JkfSA9IHVzZUF1dGgoKVxyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUoXCJcIilcclxuICBjb25zdCBbbWVzc2FnZSwgc2V0TWVzc2FnZV0gPSB1c2VTdGF0ZShcIlwiKVxyXG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKVxyXG5cclxuICBhc3luYyBmdW5jdGlvbiBoYW5kbGVTdWJtaXQoZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIHRyeSB7XHJcbiAgICBzZXRNZXNzYWdlKFwiXCIpXHJcbiAgICAgIHNldEVycm9yKFwiXCIpXHJcbiAgICAgIHNldExvYWRpbmcodHJ1ZSlcclxuICAgICAgYXdhaXQgcmVzZXRQYXNzd29yZChlbWFpbFJlZi5jdXJyZW50LnZhbHVlKVxyXG4gICAgICBzZXRNZXNzYWdlKFwiQ2hlY2sgeW91ciBlbWFpbFwiKVxyXG4gICAgfSBjYXRjaCB7XHJcbiAgICAgIHNldEVycm9yKFwiRmFpbGVkIHRvIHJlc2V0IHBhc3N3b3JkXCIpXHJcbiAgICB9XHJcbiAgICBzZXRMb2FkaW5nKGZhbHNlKVxyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPD5cclxuICAgIDxDYXJkPlxyXG4gICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgIDxoMiBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlciBtYi00XCI+UGFzc3dvcmQgcmVzZXQ8L2gyPlxyXG4gICAgICAgIHtlcnJvciAmJiA8QWxlcnQgc3R5bGU9e3tjb2xvcjogXCJyZWRcIn19PntlcnJvcn08L0FsZXJ0Pn1cclxuICAgICAgICA8Rm9ybSBvblN1Ym1pdD17aGFuZGxlU3VibWl0fT5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGlkPVwiZW1haWxcIj5cclxuICAgICAgICAgICAgPEZvcm0uTGFiZWw+RW1haWw8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2wgdHlwZT1cImVtYWlsXCIgcmVmPXtlbWFpbFJlZn0gcmVxdWlyZWQgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxCdXR0b24gZGlzYWJsZWQ9e2xvYWRpbmd9IGNsYXNzTmFtZT1cInctMTAwXCIgdHlwZT1cInN1Ym1pdFwiPlJlc2V0IFBhc3N3b3JkPC9CdXR0b24+XHJcbiAgICAgICAgPC9Gb3JtPlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL2xvZ2luXCI+TG9naW48L0xpbms+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJ3LTEwMCB0ZXh0LWNlbnRlciBtdC0yXCI+XHJcbiAgICAgIERvbnQgaGF2ZSBhbiBhY2NvdW50PyA8TGluayB0bz1cIi9zaWdudXBcIj5TaWduIFVwITwvTGluaz5cclxuICAgIDwvZGl2PlxyXG4gICAgPC8+XHJcbiAgKVxyXG59XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCB7IENvbnRhaW5lciB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCdcclxuaW1wb3J0IHt1c2VBdXRofSBmcm9tICcuL2NvbnRleHRzL0F1dGhDb250ZXh0J1xyXG5pbXBvcnQge2RifSBmcm9tICcuL2ZpcmUuanMnXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9IZWFkZXIuanMnXHJcbmltcG9ydCBNZW51IGZyb20gJy4vTWVudS5qcydcclxuaW1wb3J0IFByZWRpY3Rpb25Gb3JtIGZyb20gJy4vUHJlZGljdGlvbkZvcm0uanMnXHJcblxyXG5jb25zdCBHYW1lRGV0YWlsID0gKCkgPT57XHJcbiAgICBcclxuICAgIGNvbnN0IHtjdXJyZW50VXNlciwgbG9nb3V0fSA9IHVzZUF1dGgoKVxyXG4gICAgY29uc3QgW2dhbWVzLCBzZXRHYW1lc10gPSB1c2VTdGF0ZSh7fSlcclxuICAgIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKVxyXG5cclxuICAgIGNvbnN0IGdldE1hdGNoRGF0YSA9ICgpID0+IHtcclxuICAgICAgICBkYi5yZWYoXCJtYXRjaGVzL1wiKS5vcmRlckJ5Q2hpbGQoXCJkYXRlVGltZVwiKS5vbihcInZhbHVlXCIsIChzbmFwc2hvdCkgPT4ge1xyXG4gICAgICAgICAgICBzZXRHYW1lcyhzbmFwc2hvdC52YWwoKSlcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhzbmFwc2hvdC52YWwoKSlcclxuICAgICAgICAgICAgc2V0TG9hZGluZyh0cnVlKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgcmV0dXJuICgpID0+IGRiLnJlZi5vZmYoKVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICB1c2VFZmZlY3QoKCk9PntcclxuICAgICAgICAgICAgZ2V0TWF0Y2hEYXRhKClcclxuICAgIH0sW10pXHJcbiAgICBpZighbG9hZGluZyl7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPGRpdj5Mb2FkaW5nPC9kaXY+XHJcbiAgICAgICAgKVxyXG4gICAgfWVsc2V7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgPEhlYWRlciAvPlxyXG4gICAgICAgICAgICA8TWVudSAvPlxyXG4gICAgICAgICAgICA8Q29udGFpbmVyIHN0eWxlPXt7ZGlzcGxheTogJ2luaGVyaXQnfX0+XHJcbiAgICAgICAgICAgIHtPYmplY3Qua2V5cyhnYW1lcykubWFwKChpdGVtLCBpKSA9PiAoXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGtleT17aX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGg0IGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj4gICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGNsYXNzTmFtZT1cImZsYWdcIiBzcmM9XCJcIi8+IHtnYW1lc1tpdGVtXS5ob21lVGVhbX0gLSB7Z2FtZXNbaXRlbV0uYXdheVRlYW19IDxpbWcgY2xhc3NOYW1lPVwiZmxhZ1wiIHNyYz1cIlwiLz5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9oND5cclxuICAgICAgICAgICAgICAgIDxwPntuZXcgRGF0ZShnYW1lc1tpdGVtXS5kYXRlVGltZSAqIDEwMDApLnRvTG9jYWxlU3RyaW5nKCl9PC9wPlxyXG4gICAgICAgICAgICB7IWdhbWVzW2l0ZW1dLmZpbmlzaGVkICYmIGN1cnJlbnRVc2VyICYmXHJcbiAgICAgICAgICAgIDxQcmVkaWN0aW9uRm9ybS8+fVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICkpfVxyXG4gICAgICAgICAgICAgICAgPC9Db250YWluZXI+XHJcbiAgICAgICAgICAgIDwvPlxyXG4gICAgICAgIClcclxuICAgIH1cclxufVxyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEdhbWVEZXRhaWwiLCJpbXBvcnQgUmVhY3QsIHt1c2VDb250ZXh0LCB1c2VTdGF0ZX0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCB7dXNlQXV0aH0gZnJvbSAnLi9jb250ZXh0cy9BdXRoQ29udGV4dCdcclxuaW1wb3J0IHtMaW5rLCB1c2VIaXN0b3J5fSBmcm9tICdyZWFjdC1yb3V0ZXItZG9tJ1xyXG5cclxuY29uc3QgSGVhZGVyID0gKCkgPT4ge1xyXG5cclxuICAgIGNvbnN0IHtjdXJyZW50VXNlciwgbG9nb3V0fSA9IHVzZUF1dGgoKVxyXG4gICAgcmV0dXJuKFxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwianVtYm90cm9uIGp1bWJvdHJvbmhlaWdodFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtMTIgY29sLXNtLTEwIHRleHQtbGctcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XCIvc3RhdGljL2xvZ290L1VFRkEtRXVyby0yMDIwLnBuZ1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Y3VycmVudFVzZXIgJiYgPGg1PkxvZ2dlZCBpbiBhczoge2N1cnJlbnRVc2VyLmRpc3BsYXlOYW1lfTwvaDU+fSAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuXHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApXHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgSGVhZGVyIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgSGVhZGVyIGZyb20gJy4vSGVhZGVyJ1xyXG5pbXBvcnQgTWVudSAgZnJvbSAnLi9NZW51J1xyXG5cclxuY29uc3QgaW5kZXggPSAoKSA9PiB7XHJcbiAgICByZXR1cm4oXHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgPEhlYWRlciAvPlxyXG4gICAgICAgICAgICA8TWVudSAvPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDMgY2xhc3NOYW1lPVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFdlbGNvbWUgdG8gdGhlIEV1cm8yMDIwIHNjb3JlIHByZWRpY3Rpb24gYXBwLlxyXG4gICAgICAgICAgICAgICAgICAgIDwvaDM+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgXHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGluZGV4IiwiaW1wb3J0IFJlYWN0LCB7dXNlU3RhdGUsIHVzZVJlZn0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9IZWFkZXInXHJcbmltcG9ydCBNZW51ICBmcm9tICcuL01lbnUnXHJcbmltcG9ydCB7Q2FyZCwgRm9ybSwgQnV0dG9uLCBBbGVydCwgQ29udGFpbmVyfSBmcm9tICdyZWFjdC1ib290c3RyYXAnXHJcbmltcG9ydCB7dXNlQXV0aH0gZnJvbSAnLi9jb250ZXh0cy9BdXRoQ29udGV4dCdcclxuaW1wb3J0IHsgTGluaywgdXNlSGlzdG9yeSB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXHJcblxyXG5jb25zdCBMb2dpbiA9ICgpID0+IHtcclxuICBjb25zdCBlbWFpbFJlZiA9IHVzZVJlZigpXHJcbiAgY29uc3QgcGFzc3dvcmRSZWYgPSB1c2VSZWYoKVxyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUoXCJcIilcclxuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSlcclxuICBjb25zdCB7bG9naW59ID0gdXNlQXV0aCgpXHJcbiAgY29uc3QgaGlzdG9yeSA9IHVzZUhpc3RvcnkoKVxyXG5cclxuICBhc3luYyBmdW5jdGlvbiBoYW5kbGVTdWJtaXQoZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIHRyeSB7XHJcbiAgICAgIHNldEVycm9yKFwiXCIpXHJcbiAgICAgIHNldExvYWRpbmcodHJ1ZSlcclxuICAgICAgYXdhaXQgbG9naW4oZW1haWxSZWYuY3VycmVudC52YWx1ZSwgcGFzc3dvcmRSZWYuY3VycmVudC52YWx1ZSlcclxuICAgICAgaGlzdG9yeS5wdXNoKFwiL1wiKVxyXG4gICAgfSBjYXRjaCB7XHJcbiAgICAgIHNldEVycm9yKFwiRmFpbGVkIHRvIGxvZyBpblwiKVxyXG4gICAgfVxyXG4gICAgc2V0TG9hZGluZyhmYWxzZSlcclxuICB9XHJcbiAgcmV0dXJuIChcclxuICAgIDw+XHJcbiAgICA8SGVhZGVyLz5cclxuICAgIDxNZW51Lz5cclxuICAgIDxDb250YWluZXI+XHJcbiAgICA8Q2FyZD5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8aDIgY2xhc3NOYW1lPVwidGV4dC1jZW50ZXIgbWItNFwiPkxvZyBJbjwvaDI+XHJcbiAgICAgICAge2Vycm9yICYmIDxBbGVydCBzdHlsZT17e2NvbG9yOiBcInJlZFwifX0+e2Vycm9yfTwvQWxlcnQ+fVxyXG4gICAgICAgIDxGb3JtIG9uU3VibWl0PXtoYW5kbGVTdWJtaXR9PlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJlbWFpbFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwiZW1haWxcIiByZWY9e2VtYWlsUmVmfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJwYXNzd29yZFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5QYXNzd29yZDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwicGFzc3dvcmRcIiByZWY9e3Bhc3N3b3JkUmVmfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEJ1dHRvbiBkaXNhYmxlZD17bG9hZGluZ30gY2xhc3NOYW1lPVwidy0xMDBcIiB0eXBlPVwic3VibWl0XCI+TG9nIEluPC9CdXR0b24+XHJcbiAgICAgICAgPC9Gb3JtPlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxMaW5rIHRvPVwiL2ZvcmdvdC1wYXNzd29yZFwiPkZvcmdvdCBQYXNzd29yZD88L0xpbms+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJ3LTEwMCB0ZXh0LWNlbnRlciBtdC0yXCI+XHJcbiAgICAgIERvbnQgaGF2ZSBhbiBhY2NvdW50PyA8TGluayB0bz1cIi9zaWdudXBcIj5TaWduIFVwITwvTGluaz5cclxuICAgIDwvZGl2PlxyXG4gICAgPC9Db250YWluZXI+XHJcbiAgICA8Lz5cclxuICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IExvZ2luIiwiaW1wb3J0IFJlYWN0LCB7dXNlU3RhdGV9IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQge3VzZUF1dGh9IGZyb20gJy4vY29udGV4dHMvQXV0aENvbnRleHQnXHJcbmltcG9ydCB7TGluaywgdXNlSGlzdG9yeX0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSdcclxuXHJcbmNvbnN0IE1lbnUgPSAoKSA9PiB7XHJcbiAgICBcclxuICAgIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUoXCJcIilcclxuICAgIGNvbnN0IHtjdXJyZW50VXNlciwgbG9nb3V0fSA9IHVzZUF1dGgoKVxyXG4gICAgY29uc3QgaGlzdG9yeSA9IHVzZUhpc3RvcnkoKVxyXG4gICAgYXN5bmMgZnVuY3Rpb24gaGFuZGxlTG9nb3V0KCl7XHJcbiAgICAgICAgc2V0RXJyb3IoXCJcIilcclxuICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgIGF3YWl0IGxvZ291dCgpXHJcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaChcIi9sb2dpblwiKVxyXG4gICAgICAgIH1jYXRjaHtcclxuICAgICAgICAgICAgc2V0RXJyb3IoXCJGYWlsZWQgdG8gbG9nb3V0XCIpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuKFxyXG4gICAgICAgIDxuYXYgc3R5bGU9e3tkaXNwbGF5OiAnaW5saW5lLWJsb2NrJ319PlxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBIb21lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbWF0Y2hlc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTWF0Y2hlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgICB7Y3VycmVudFVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL2Rhc2hib2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRGFzaGJvYXJkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHshY3VycmVudFVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvbG9naW5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIExvZ2luXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Y3VycmVudFVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgPExpbmsgdG89XCIvXCIgb25DbGljaz17aGFuZGxlTG9nb3V0fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIExvZ291dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE1lbnUiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXHJcbmNvbnN0IFByZWRpY3Rpb25Gb3JtID0gKCkgPT4ge1xyXG4gICAgXHJcbiAgICBmdW5jdGlvbiBwcmVkaWN0aW9uU2F2ZUhhbmRsZXIoZXZlbnQpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTdWJtaXR0ZWRcIilcclxuICAgIH1cclxuICAgICAgICByZXR1cm4oXHJcbiAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17cHJlZGljdGlvblNhdmVIYW5kbGVyfT5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3NOYW1lPVwic2NvcmVpbnB1dFwiIHR5cGU9XCJudW1iZXJcIiByZXF1aXJlZD48L2lucHV0PlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJzY29yZWlucHV0XCIgdHlwZT1cIm51bWJlclwiIHJlcXVpcmVkPjwvaW5wdXQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCI+U2F2ZTwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApXHJcbiAgICBcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUHJlZGljdGlvbkZvcm0iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXHJcbmltcG9ydCB7UmVkaXJlY3QsIFJvdXRlfSBmcm9tICdyZWFjdC1yb3V0ZXItZG9tJ1xyXG5pbXBvcnQge3VzZUF1dGh9IGZyb20gJy4vY29udGV4dHMvQXV0aENvbnRleHQnXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFByaXZhdGVSb3V0ZSh7Y29tcG9uZW50OiBDb21wb25lbnQsIC4uLnJlc3R9KXtcclxuICAgIGNvbnN0IHtjdXJyZW50VXNlcn0gPSB1c2VBdXRoKClcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPFJvdXRlXHJcbiAgICAgICAgICAgIHsuLi5yZXN0fVxyXG4gICAgICAgICAgICByZW5kZXI9e3Byb3BzID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50VXNlciA/IDxDb21wb25lbnQgey4uLnByb3BzfSAvPiA6IDxSZWRpcmVjdCB0bz1cIi9cIiAvPlxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgID48L1JvdXRlPlxyXG4gICAgKVxyXG59IiwiaW1wb3J0IFJlYWN0LCB7dXNlU3RhdGUsIHVzZVJlZn0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9IZWFkZXInXHJcbmltcG9ydCBNZW51ICBmcm9tICcuL01lbnUnXHJcbmltcG9ydCB7Q2FyZCwgRm9ybSwgQnV0dG9uLCBBbGVydCwgQ29udGFpbmVyfSBmcm9tICdyZWFjdC1ib290c3RyYXAnXHJcbmltcG9ydCB7dXNlQXV0aH0gZnJvbSAnLi9jb250ZXh0cy9BdXRoQ29udGV4dCdcclxuaW1wb3J0IHsgTGluayB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXHJcblxyXG5jb25zdCBTaWduVXAgPSAoKSA9PiB7XHJcbiAgY29uc3QgZW1haWxSZWYgPSB1c2VSZWYoKVxyXG4gIGNvbnN0IHBhc3N3b3JkUmVmID0gdXNlUmVmKClcclxuICBjb25zdCBwYXNzd29yZENvbmZpcm1SZWYgPSB1c2VSZWYoKVxyXG4gIGNvbnN0IFtlcnJvciwgc2V0RXJyb3JdID0gdXNlU3RhdGUoXCJcIilcclxuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSlcclxuICBjb25zdCB7c2lnbnVwfSA9IHVzZUF1dGgoKVxyXG5cclxuICBhc3luYyBmdW5jdGlvbiBoYW5kbGVTdWJtaXQoZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIGlmKHBhc3N3b3JkUmVmLmN1cnJlbnQudmFsdWUgIT09IHBhc3N3b3JkQ29uZmlybVJlZi5jdXJyZW50LnZhbHVlKXtcclxuICAgICAgcmV0dXJuIHNldEVycm9yKFwiUGFzc3dvcmRzIGRvIG5vdCBtYXRjaFwiKVxyXG4gICAgfVxyXG4gICAgdHJ5IHtcclxuICAgICAgc2V0RXJyb3IoXCJcIilcclxuICAgICAgc2V0TG9hZGluZyh0cnVlKVxyXG4gICAgICBhd2FpdCBzaWdudXAoZW1haWxSZWYuY3VycmVudC52YWx1ZSwgcGFzc3dvcmRSZWYuY3VycmVudC52YWx1ZSlcclxuICAgIH0gY2F0Y2gge1xyXG4gICAgICBzZXRFcnJvcihcIkZhaWxlZCB0byBzaWduIHVwXCIpXHJcbiAgICB9XHJcbiAgICBzZXRMb2FkaW5nKGZhbHNlKVxyXG4gIH1cclxuICByZXR1cm4gKFxyXG4gICAgPD5cclxuICAgIDxIZWFkZXIvPlxyXG4gICAgPE1lbnUvPlxyXG4gICAgPENvbnRhaW5lcj5cclxuICAgIDxDYXJkPlxyXG4gICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgIDxoMiBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlciBtYi00XCI+U2lnbiBVcDwvaDI+XHJcbiAgICAgICAge2Vycm9yICYmIDxBbGVydCBzdHlsZT17e2NvbG9yOiBcInJlZFwifX0+e2Vycm9yfTwvQWxlcnQ+fVxyXG4gICAgICAgIDxGb3JtIG9uU3VibWl0PXtoYW5kbGVTdWJtaXR9PlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJlbWFpbFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwiZW1haWxcIiByZWY9e2VtYWlsUmVmfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJwYXNzd29yZFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5QYXNzd29yZDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwicGFzc3dvcmRcIiByZWY9e3Bhc3N3b3JkUmVmfSByZXF1aXJlZCAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJwYXNzd29yZC1jb25maXJtXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPlBhc3N3b3JkIENvbmZpcm1hdGlvbjwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwicGFzc3dvcmRcIiByZWY9e3Bhc3N3b3JkQ29uZmlybVJlZn0gcmVxdWlyZWQgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxCdXR0b24gZGlzYWJsZWQ9e2xvYWRpbmd9IGNsYXNzTmFtZT1cInctMTAwXCIgdHlwZT1cInN1Ym1pdFwiPlNpZ24gVXA8L0J1dHRvbj5cclxuICAgICAgICA8L0Zvcm0+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICAgXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIG10LTJcIj5cclxuICAgICAgQWxyZWFkeSBoYXZlIGFuIGFjY291bnQ/IDxMaW5rIHRvPVwibG9naW5cIj5Mb2cgSW4hPC9MaW5rPlxyXG4gICAgPC9kaXY+XHJcbiAgICA8L0NvbnRhaW5lcj5cclxuICAgIDwvPlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgU2lnblVwIiwiaW1wb3J0IFJlYWN0LCB7dXNlU3RhdGUsIHVzZVJlZn0gZnJvbSAncmVhY3QnXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9IZWFkZXInXHJcbmltcG9ydCBNZW51ICBmcm9tICcuL01lbnUnXHJcbmltcG9ydCB7Q2FyZCwgRm9ybSwgQnV0dG9uLCBBbGVydCwgQ29udGFpbmVyfSBmcm9tICdyZWFjdC1ib290c3RyYXAnXHJcbmltcG9ydCB7dXNlQXV0aH0gZnJvbSAnLi9jb250ZXh0cy9BdXRoQ29udGV4dCdcclxuaW1wb3J0IHsgTGluaywgdXNlSGlzdG9yeSB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXHJcblxyXG5jb25zdCBVcGRhdGVQcm9maWxlID0gKCkgPT4ge1xyXG4gIGNvbnN0IGVtYWlsUmVmID0gdXNlUmVmKClcclxuICBjb25zdCBwYXNzd29yZFJlZiA9IHVzZVJlZigpXHJcbiAgY29uc3QgZGlzcGxheU5hbWVSZWYgPSB1c2VSZWYoKVxyXG4gIGNvbnN0IHBhc3N3b3JkQ29uZmlybVJlZiA9IHVzZVJlZigpXHJcbiAgY29uc3QgW2Vycm9yLCBzZXRFcnJvcl0gPSB1c2VTdGF0ZShcIlwiKVxyXG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKVxyXG4gIGNvbnN0IHtjdXJyZW50VXNlciwgdXBkYXRlUGFzc3dvcmQsIHVwZGF0ZUVtYWlsLCB1cGRhdGVEaXNwbGF5TmFtZX0gPSB1c2VBdXRoKClcclxuICBjb25zdCBoaXN0b3J5ID0gdXNlSGlzdG9yeSgpXHJcblxyXG4gIGZ1bmN0aW9uIGhhbmRsZVN1Ym1pdChlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIGlmKHBhc3N3b3JkUmVmLmN1cnJlbnQudmFsdWUgIT09IHBhc3N3b3JkQ29uZmlybVJlZi5jdXJyZW50LnZhbHVlKXtcclxuICAgICAgcmV0dXJuIHNldEVycm9yKFwiUGFzc3dvcmRzIGRvIG5vdCBtYXRjaFwiKVxyXG4gICAgfVxyXG4gIFxyXG4gIGNvbnN0IHByb21pc2VzID0gW11cclxuICAgIHNldExvYWRpbmcodHJ1ZSlcclxuICAgIHNldEVycm9yKFwiXCIpXHJcbiAgaWYoZW1haWxSZWYuY3VycmVudC52YWx1ZSAhPT0gY3VycmVudFVzZXIuZW1haWwpe1xyXG4gICAgICBwcm9taXNlcy5wdXNoKHVwZGF0ZUVtYWlsKGVtYWlsUmVmLmN1cnJlbnQudmFsdWUpKVxyXG4gIH1cclxuICBpZihwYXNzd29yZFJlZi5jdXJyZW50LnZhbHVlKXtcclxuICAgIHByb21pc2VzLnB1c2godXBkYXRlUGFzc3dvcmQocGFzc3dvcmRSZWYuY3VycmVudC52YWx1ZSkpXHJcbiAgfVxyXG4gIGlmKGRpc3BsYXlOYW1lUmVmLmN1cnJlbnQudmFsdWUgIT09IGN1cnJlbnRVc2VyLmRpc3BsYXlOYW1lKXtcclxuICAgIHByb21pc2VzLnB1c2godXBkYXRlRGlzcGxheU5hbWUoZGlzcGxheU5hbWVSZWYuY3VycmVudC52YWx1ZSkpXHJcbiAgfVxyXG4gICAgUHJvbWlzZS5hbGwocHJvbWlzZXMpXHJcbiAgICAudGhlbigoKT0+e1xyXG4gICAgICAgIGhpc3RvcnkucHVzaChcIi9cIilcclxuICAgIH0pLmNhdGNoKCgpPT57XHJcbiAgICAgICAgc2V0RXJyb3IoXCJGYWlsZWQgdG8gdXBkYXRlIGFjY291bnRcIilcclxuICAgIH0pLmZpbmFsbHkoKCk9PntcclxuICAgICAgICBzZXRMb2FkaW5nKGZhbHNlKVxyXG4gICAgfSlcclxuICB9XHJcbiAgcmV0dXJuIChcclxuICAgIDw+XHJcbiAgICA8SGVhZGVyLz5cclxuICAgIDxNZW51Lz5cclxuICAgIDxDb250YWluZXI+XHJcbiAgICA8Q2FyZD5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8aDIgY2xhc3NOYW1lPVwidGV4dC1jZW50ZXIgbWItNFwiPlVwZGF0ZSBQcm9maWxlPC9oMj5cclxuICAgICAgICB7ZXJyb3IgJiYgPEFsZXJ0IHN0eWxlPXt7Y29sb3I6IFwicmVkXCJ9fT57ZXJyb3J9PC9BbGVydD59XHJcbiAgICAgICAgPEZvcm0gb25TdWJtaXQ9e2hhbmRsZVN1Ym1pdH0+XHJcbiAgICAgICAgICA8Rm9ybS5Hcm91cCBpZD1cImVtYWlsXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPk5pY2tuYW1lPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sIHR5cGU9XCJ0ZXh0XCIgcmVmPXtkaXNwbGF5TmFtZVJlZn0gZGVmYXVsdFZhbHVlPXtjdXJyZW50VXNlci5kaXNwbGF5TmFtZX0gLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGlkPVwiZW1haWxcIj5cclxuICAgICAgICAgICAgPEZvcm0uTGFiZWw+RW1haWw8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2wgdHlwZT1cImVtYWlsXCIgcmVmPXtlbWFpbFJlZn0gZGVmYXVsdFZhbHVlPXtjdXJyZW50VXNlci5lbWFpbH0gLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGlkPVwicGFzc3dvcmRcIj5cclxuICAgICAgICAgICAgPEZvcm0uTGFiZWw+UGFzc3dvcmQ8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2wgdHlwZT1cInBhc3N3b3JkXCIgcmVmPXtwYXNzd29yZFJlZn0gcGxhY2Vob2xkZXI9XCJMZWF2ZSBibGFuayB0byBrZWVwIHRoZSBzYW1lXCIvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgaWQ9XCJwYXNzd29yZC1jb25maXJtXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPlBhc3N3b3JkIENvbmZpcm1hdGlvbjwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbCB0eXBlPVwicGFzc3dvcmRcIiByZWY9e3Bhc3N3b3JkQ29uZmlybVJlZn0gcGxhY2Vob2xkZXI9XCJMZWF2ZSBibGFuayB0byBrZWVwIHRoZSBzYW1lXCIvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgPEJ1dHRvbiBkaXNhYmxlZD17bG9hZGluZ30gY2xhc3NOYW1lPVwidy0xMDBcIiB0eXBlPVwic3VibWl0XCI+VXBkYXRlPC9CdXR0b24+XHJcbiAgICAgICAgPC9Gb3JtPlxyXG4gICAgICA8L0NhcmQuQm9keT5cclxuICAgIDwvQ2FyZD5cclxuICAgIDxkaXYgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgbXQtMlwiPlxyXG4gICAgICA8TGluayB0bz1cIi9cIj5DYW5jZWw8L0xpbms+XHJcbiAgICA8L2Rpdj5cclxuICAgIDwvQ29udGFpbmVyPlxyXG4gICAgPC8+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBVcGRhdGVQcm9maWxlIiwiaW1wb3J0IFJlYWN0LCB7IHVzZUNvbnRleHQsIHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCdcclxuaW1wb3J0IHthdXRofSBmcm9tICcuLi9maXJlJ1xyXG5cclxuY29uc3QgQXV0aENvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KClcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiB1c2VBdXRoKCl7XHJcbiAgICByZXR1cm4gdXNlQ29udGV4dChBdXRoQ29udGV4dClcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIEF1dGhQcm92aWRlcih7Y2hpbGRyZW59KXtcclxuXHJcbiAgICBjb25zdCBbY3VycmVudFVzZXIsIHNldEN1cnJlbnRVc2VyXT11c2VTdGF0ZSgpXHJcbiAgICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ109dXNlU3RhdGUodHJ1ZSlcclxuXHJcbiAgICBmdW5jdGlvbiBzaWdudXAoZW1haWwscGFzc3dvcmQpe1xyXG4gICAgICAgIHJldHVybiBhdXRoLmNyZWF0ZVVzZXJXaXRoRW1haWxBbmRQYXNzd29yZChlbWFpbCxwYXNzd29yZClcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIGxvZ2luKGVtYWlsLHBhc3N3b3JkKXtcclxuICAgICAgICByZXR1cm4gYXV0aC5zaWduSW5XaXRoRW1haWxBbmRQYXNzd29yZChlbWFpbCxwYXNzd29yZClcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIGxvZ291dCgpe1xyXG4gICAgICAgIHJldHVybiBhdXRoLnNpZ25PdXQoKVxyXG4gICAgfVxyXG4gICAgZnVuY3Rpb24gcmVzZXRQYXNzd29yZChlbWFpbCl7XHJcbiAgICAgICAgcmV0dXJuIGF1dGguc2VuZFBhc3N3b3JkUmVzZXRFbWFpbChlbWFpbClcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIHVwZGF0ZUVtYWlsKGVtYWlsKXtcclxuICAgICAgICByZXR1cm4gY3VycmVudFVzZXIudXBkYXRlRW1haWwoZW1haWwpXHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiB1cGRhdGVQYXNzd29yZChwYXNzd29yZCl7XHJcbiAgICAgICAgcmV0dXJuIGN1cnJlbnRVc2VyLnVwZGF0ZVBhc3N3b3JkKHBhc3N3b3JkKVxyXG4gICAgfVxyXG4gICAgZnVuY3Rpb24gdXBkYXRlRGlzcGxheU5hbWUoZGlzcGxheU5hbWUpe1xyXG4gICAgICAgIHJldHVybiBjdXJyZW50VXNlci51cGRhdGVQcm9maWxlKHtkaXNwbGF5TmFtZTogZGlzcGxheU5hbWV9KVxyXG4gICAgfVxyXG4gICAgdXNlRWZmZWN0KCgpID0+e1xyXG4gICAgICAgIGNvbnN0IHVuc3Vic2NyaWJlID0gYXV0aC5vbkF1dGhTdGF0ZUNoYW5nZWQodXNlciA9PiB7XHJcbiAgICAgICAgICAgIHNldEN1cnJlbnRVc2VyKHVzZXIpXHJcbiAgICAgICAgICAgIHNldExvYWRpbmcoZmFsc2UpXHJcbiAgICAgICAgfSlcclxuICAgICAgICByZXR1cm4gdW5zdWJzY3JpYmVcclxuICAgIH0sW10pXHJcbiAgICBcclxuXHJcbiAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICBjdXJyZW50VXNlcixcclxuICAgICAgICBsb2dpbixcclxuICAgICAgICBzaWdudXAsXHJcbiAgICAgICAgbG9nb3V0LFxyXG4gICAgICAgIHJlc2V0UGFzc3dvcmQsXHJcbiAgICAgICAgdXBkYXRlRW1haWwsXHJcbiAgICAgICAgdXBkYXRlUGFzc3dvcmQsXHJcbiAgICAgICAgdXBkYXRlRGlzcGxheU5hbWVcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4oPGRpdj5cclxuICAgICAgICA8QXV0aENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3ZhbHVlfT5cclxuICAgICAgICAgICAgeyFsb2FkaW5nICYmIGNoaWxkcmVufVxyXG4gICAgICAgICAgICB7Y2hpbGRyZW59XHJcbiAgICAgICAgPC9BdXRoQ29udGV4dC5Qcm92aWRlcj5cclxuICAgIDwvZGl2PilcclxufVxyXG4iLCJpbXBvcnQgZmlyZWJhc2UgZnJvbSAnZmlyZWJhc2UvYXBwJ1xyXG5pbXBvcnQgXCJmaXJlYmFzZS9hdXRoXCJcclxuaW1wb3J0IFwiZmlyZWJhc2UvZGF0YWJhc2VcIlxyXG52YXIgZmlyZTtcclxudmFyIGZpcmViYXNlQ29uZmlnID0ge1xyXG4gIGFwaUtleTogXCJBSXphU3lCSTgxLTdsd2o4dk40UDFKejh5QU5MZFRLdzl1UU5DZ2NcIixcclxuICBhdXRoRG9tYWluOiBcImVtdmVpa2thdXMtZWQ5ZTQuZmlyZWJhc2VhcHAuY29tXCIsXHJcbiAgZGF0YWJhc2VVUkw6IFwiaHR0cHM6Ly9lbXZlaWtrYXVzLWVkOWU0LWRlZmF1bHQtcnRkYi5ldXJvcGUtd2VzdDEuZmlyZWJhc2VkYXRhYmFzZS5hcHBcIixcclxuICBwcm9qZWN0SWQ6IFwiZW12ZWlra2F1cy1lZDllNFwiLFxyXG4gIHN0b3JhZ2VCdWNrZXQ6IFwiZW12ZWlra2F1cy1lZDllNC5hcHBzcG90LmNvbVwiLFxyXG4gIG1lc3NhZ2luZ1NlbmRlcklkOiBcIjEwNTkyMDE2MzU1NDBcIixcclxuICBhcHBJZDogXCIxOjEwNTkyMDE2MzU1NDA6d2ViOmEwOWRhZDI2MDhjYjQ3N2E0NDk3NDFcIixcclxuICBtZWFzdXJlbWVudElkOiBcIkctTTdaTEtFME5KRlwiXHJcbn07XHJcbiAgaWYgKCFmaXJlYmFzZS5hcHBzLmxlbmd0aCkge1xyXG4gICAgZmlyZSA9IGZpcmViYXNlLmluaXRpYWxpemVBcHAoZmlyZWJhc2VDb25maWcpO1xyXG4gfSAgZWxzZSB7XHJcbiAgICBmaXJlID0gZmlyZWJhc2UuYXBwKCk7IC8vIGlmIGFscmVhZHkgaW5pdGlhbGl6ZWQsIHVzZSB0aGF0IG9uZVxyXG4gfVxyXG4vL2NvbnN0IGZpcmUgPSBmaXJlYmFzZS5pbml0aWFsaXplQXBwKGZpcmViYXNlQ29uZmlnKTtcclxuLy9leHBvcnQgY29uc3QgYXV0aCA9IGZpcmUuYXV0aCgpXHJcbi8vZXhwb3J0IGNvbnN0IGRiID0gZmlyZWJhc2UuZGF0YWJhc2UoKVxyXG5jb25zdCBhdXRoID0gZmlyZS5hdXRoKClcclxuY29uc3QgZGIgPSBmaXJlLmRhdGFiYXNlKClcclxuZXhwb3J0IHtcclxuICBhdXRoLFxyXG4gIGRiXHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgZmlyZSIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImZpcmViYXNlL2FwcFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvYXV0aFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvZGF0YWJhc2VcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1ib290c3RyYXBcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LXJvdXRlci1kb21cIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTs7Il0sInNvdXJjZVJvb3QiOiIifQ==