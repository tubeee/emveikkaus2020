import firebase from 'firebase/app'
import "firebase/auth"
import "firebase/database"
var fire;

var firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
};

  if (!firebase.apps.length) {
    fire = firebase.initializeApp(firebaseConfig);
 }  else {
    fire = firebase.app(); // if already initialized, use that one
 }
//const fire = firebase.initializeApp(firebaseConfig);
//export const auth = fire.auth()
//export const db = firebase.database()
const auth = fire.auth()
const db = fire.database()
export {
  auth,
  db
}
export default fire