import React, { useContext, useEffect, useState } from 'react'
import { auth, db } from '../fire'

const AuthContext = React.createContext()

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {

    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)
    
    async function signup(email, password, displayName) {
        return auth.createUserWithEmailAndPassword(email, password)
        .then(currentUser => {
            const userInformation = {
                bio: "empty",
                score: 0,
                isAdmin: false,
                displayName: displayName
            }
            db.ref("users/" + currentUser.user.uid).set(userInformation)
            .then(() =>{
                currentUser.user.updateProfile({displayName: displayName})
            })
        })
    }
    async function login(email, password) {
        return auth.signInWithEmailAndPassword(email, password)
    }
    function logout() {
        return auth.signOut()
    }
    function resetPassword(email) {
        return auth.sendPasswordResetEmail(email)
    }
    function updateEmail(email) {
        return currentUser.updateEmail(email)
    }
    function updatePassword(password) {
        return currentUser.updatePassword(password)
    }
    async function updateDisplayName(displayName) {
        return currentUser.updateProfile({ displayName: displayName })
    }

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            setCurrentUser(user)
            setLoading(false)
        })
        return unsubscribe
    }, [])


    const value = {
        currentUser,
        login,
        signup,
        logout,
        resetPassword,
        updateEmail,
        updatePassword,
        updateDisplayName
    }

    return (
        <div>
            <AuthContext.Provider value={value}>
                {!loading && children}
            </AuthContext.Provider>
        </div>)
}
