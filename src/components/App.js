import React from 'react'
import SignUp from './SignUp'
import Login from './Login'
import { AuthProvider } from '../contexts/AuthContext.js'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Dashboard from './Dashboard'
import PrivateRoute from './PrivateRoute'
import ForgotPassword from './ForgotPassword'
import UpdateProfile from './UpdateProfile'
import GameDetail from './GameDetail'
import Home from './Home'
import 'bootstrap/dist/css/bootstrap.min.css'
import Leaderboard from './Leaderboard'
import Results from './Results'

const App = () => {
    return (
        <>
                <Router>
                    <AuthProvider>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <PrivateRoute path="/update-profile" component={UpdateProfile} />
                            <PrivateRoute path="/dashboard" component={Dashboard} />
                            <Route path="/signup" component={SignUp} />
                            <Route path="/login" component={Login} />
                            <Route path="/forgot-password" component={ForgotPassword} />
                            <Route path="/matches" component={GameDetail} />
                            <Route path="/results" component={Results} />
                            <Route path="/leaderboard" component={Leaderboard} />
                        </Switch>
                    </AuthProvider>
                </Router>
        </>
    )
}
export default App