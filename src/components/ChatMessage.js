import React from 'react'

const ChatMessage = (props) => {

    const { text, displayName, time } = props.message;
    let timeF = new Date(time * 1000).toLocaleString()
    
    return (
                <div className="card-body border-bottom">
                    <div className="d-flex justify-content-start mb-4">
                        <span><b>{displayName}: </b></span>
                        <div className="container">
                            <p className="">{text}</p>
                            <span style={{ fontSize: 10 }}>{timeF}</span>
                        </div>
                    </div>
                </div>
    )
}

export default ChatMessage