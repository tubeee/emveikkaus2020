import React, { useState, useRef } from 'react'
import Header from './Header'
import Menu from './Menu'
import { Card, Form, Button, Alert, Container } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext.js'
import { Link, useHistory } from 'react-router-dom'

const Login = () => {
  const emailRef = useRef()
  const passwordRef = useRef()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const { login } = useAuth()
  const history = useHistory()

  async function handleSubmit(e) {
    e.preventDefault()
    setLoading(true)
    try {
      setError("")
      await login(emailRef.current.value, passwordRef.current.value).then(()=>{
        setLoading(false)
      })
      history.push("/")
    } catch {
      setError("Failed to log in")
    }
  }
  return (
    <>
      <Header />
      <Container>
        <Card>
          <Card.Body>
            <h2 className="text-center mb-4">Log In</h2>
            {error && <Alert style={{ color: "red" }}>{error}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" ref={emailRef} required />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" ref={passwordRef} required />
              </Form.Group>
              <Button disabled={loading} className="w-100" type="submit">Log In</Button>
            </Form>
            <div>
              <Link to="/forgot-password">Forgot Password?</Link>
            </div>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
          Dont have an account? <Link to="/signup">Sign Up!</Link>
        </div>
      </Container>
    </>
  )
}

export default Login