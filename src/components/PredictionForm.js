import React, { useState, useRef } from 'react'
import { db } from '../fire.js'
import { useHistory } from 'react-router-dom'

const PredictionForm = (props) => {
    const userPredictionRef = db.ref("user-predictions/" + props.uid)
    const matchRef = db.ref("matches/" + [props.matchId])
    const [error, setError] = useState("")
    const [loading, setLoading] = useState(false)
    const homeScoreRef = useRef()
    const awayScoreRef = useRef()
    const resultHomeRef = useRef()
    const resultAwayRef = useRef()
    const history = useHistory()
    var resultVar
    const [date] = useState(Math.floor(Date.now() / 1000))
    
    async function getUsers() {
        var user
        await db.ref("users/")
            .once("value", snapshot => {
                user = snapshot.val()
            })
        return user
    }
    const updateUserPoints = async (userId, points) => {
        const userRef = db.ref("users/" + userId + "/score")
        await userRef.transaction(function (currentData) {
            return currentData + points
        })
    }
    const updateUserMatchPoints = async (userId, points) => {
        const userPredRef = db.ref("user-predictions/" + userId + "/" + [props.matchId] + "/score")
        await userPredRef.transaction(function (currentData) {
            return currentData + points
        })
    }
    const getUserPrediction = async (userId) => {
        var prediction
        await db.ref("user-predictions/" + userId).once("value", (snapshot) => {
            if (snapshot.val()) {
                console.log("inside calculatePoints, userId: " + userId)
                prediction = snapshot.val()
            } else {
                prediction = null
            }
        })
        return prediction
    }
    const calculatePoints = (gameResult, gamePred) => {
        var pts = 0
        var currentPred = gamePred[props.matchId]
        if (currentPred !== undefined) {
            if (gameResult.result.home === currentPred.result.home) {
                console.log("homescore correct")
                pts += 1
            }
            if (gameResult.result.away === currentPred.result.away) {
                console.log("awayscore correct")
                pts += 1
            }
            if (gameResult.result.outcome === currentPred.result.outcome) {
                console.log("outcome correct")
                pts += 3
            }
        }
        return pts
    }

    const saveResultsToDb = async () => {
        setError("")
        let resultOutcome = ""

        if (resultHomeRef.current.value !== null && resultAwayRef.current.value !== null) {
            if (resultHomeRef.current.value > resultAwayRef.current.value) {
                resultOutcome = "home"
            }
            if (resultAwayRef.current.value > resultHomeRef.current.value) {
                resultOutcome = "away"
            }
            if (resultHomeRef.current.value === resultAwayRef.current.value) {
                resultOutcome = "draw"
            }
            resultVar = {
                finished: true,
                result: {
                    home: resultHomeRef.current.value,
                    away: resultAwayRef.current.value,
                    outcome: resultOutcome
                }
            }
            try {
                return await matchRef.update(resultVar)
            }
            catch {
                setError("Error saving the results")
            }
        }
    }

    async function adminSaveHandler(e) {
        e.preventDefault()
        try {
            setError("")
            setLoading(true)
            await saveResultsToDb()
            var users = await getUsers()
            console.log(users)
            for (var user in users) {
                console.log(resultVar)
                console.log("inside for-loop, userId: " + user)
                var gamepred = await getUserPrediction(user)
                if (gamepred !== null) {
                    console.log(gamepred)
                    var userPoints = calculatePoints(resultVar, gamepred)
                    console.log("user got: " + userPoints + " points")
                    if (userPoints > 0) {
                        await updateUserPoints(user, userPoints)
                        await updateUserMatchPoints(user, userPoints)
                    }
                }

            }
        } catch {
            setError("Failed to update score")
        }
        setLoading(false)
    }


    function predictionSaveHandler(e) {
        e.preventDefault()
        let resultOutcome = ""
        let datee = (Math.floor(Date.now() / 1000))
        
        if (homeScoreRef.current.value > awayScoreRef.current.value) {
            resultOutcome = "home"
        }
        if (homeScoreRef.current.value < awayScoreRef.current.value) {
            resultOutcome = "away"
        }
        if (homeScoreRef.current.value === awayScoreRef.current.value) {
            resultOutcome = "draw"
        }
        const scoreInformation = {
            [props.matchId]: {
                result: {
                    home: homeScoreRef.current.value,
                    away: awayScoreRef.current.value,
                    outcome: resultOutcome
                }
            }
        }
        if (homeScoreRef.current.value === null || awayScoreRef.current.value === null) {
            return setError("Please input both values before submitting")
        }
        try {
            setError("")
            setLoading(true)
            if(datee < props.date){
                userPredictionRef.update(scoreInformation)
            }
            history.push("/matches")
        } catch {
            setError("Failed to update predictions")
        }
        setLoading(false)
    }

    //toimii
    return (

        <div className="">
            {props.admin.isAdmin === true &&
                <>
                    <form onSubmit={adminSaveHandler}>
                        <input type="number" ref={resultHomeRef} required></input>
                        <input type="number" ref={resultAwayRef} required></input>
                        <button type="submit">Set score</button>
                    </form>
                </>
            }
            {'home' in props &&
                <div>
                    Your prediction:<br />
                    <span>{props.homeTeam}: {props.home} {props.awayTeam}: {props.away}</span>
                </div>
            }
            {!loading && !props.finish && date < props.date &&
                <form onSubmit={predictionSaveHandler} className="col-12">
                    {error && <div>{error}</div>}
                    <input className="col-3" type="number" min="0" ref={homeScoreRef} required></input>
                &#8200;&#8211;&#8200;
                <input className="col-3" type="number" min="0" ref={awayScoreRef} required></input>&#8200;
                {'home' in props ?
                        <button className="" type="submit">Update prediction</button>
                        :
                        <button type="submit">Save prediction</button>
                    }
                </form>
            }
        </div>
    )

}

export default PredictionForm