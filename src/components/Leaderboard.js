import React, { useEffect, useState } from 'react'
import Header from './Header'
import Menu from './Menu'
import { db } from '../fire.js'

const Leaderboard = () => {
    var users = []
    const [uusers, setUusers] = useState([])
    const [loading, setLoading] = useState(false)

    const getUsers = async () => {
        setLoading(true)
        await db.ref("users/").orderByChild('score').once("child_added", (snapshot) => {
            if (snapshot.val().score > 0) {
                users.push(snapshot.val())
            }
        })
        setLoading(false)
        return setUusers(users)
    }

    useEffect(() => {
        getUsers()
    }, [])

    if (loading) {
        return (<div>Loading..</div>)
    }
    return (
        <>
            <Header />

            {/* <div className="container">
                <h2 className="text-light">Leaderboard</h2>
                <ul className="list-group">
                    
                    {Object.keys(uusers).map((item, i) => (
                        
                        <li className="list-group-item" key={i}>
                            <p>{i + 1}. {uusers[item].displayName} -
                            Points: {uusers[item].score}</p>
                        </li>
                    
                    ))}
                </ul>
            </div> */}
            <div className="container">
                <h2 className="text-light">Leaderboard</h2>
                <div className="table-responsive">

                    <table className="table table-striped custom-table text-light" style={{backgroundColor: "#6A665D"}}>

                        <thead>
                            <tr>
                                <th scope="col">Ranking</th>
                                <th scope="col">Name</th>
                                <th scope="col">Points</th>
                            </tr>
                        </thead>
                        {Object.keys(uusers).map((item, i) => (

                            <tbody >
                                <tr scope="row" key={i} className="text-light">
                                    <td>{i + 1}</td>
                                    <td><b>{uusers[item].displayName}</b></td>
                                    <td><b>{uusers[item].score}</b></td>
                                </tr>
                            </tbody>
                        ))}
                    </table>
                </div>
            </div>
        </>
    )
}

export default Leaderboard