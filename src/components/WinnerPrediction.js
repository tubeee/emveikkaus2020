import React, { useEffect, useRef, useState } from 'react'
import { db } from '../fire.js'

const WinnerPrediction = (props) => {
    const winnerRef = useRef()
    const scorerRef = useRef()
    const [prediction, setPrediction] = useState()
    const [scorePrediction, setScorePrediction] = useState()
    const newDate = (Date.now() / 1000)
    const firstMatch = 1623438000
    const teams = ["Austria",
        "Belgium",
        "Croatia",
        "Czech Republic",
        "Denmark",
        "England",
        "Finland",
        "France",
        "Germany",
        "Hungary",
        "Italy",
        "Netherlands",
        "North Macedonia",
        "Poland",
        "Portugal",
        "Russia",
        "Scotland",
        "Slovakia",
        "Spain",
        "Sweden",
        "Switzerland",
        "Turkey",
        "Ukraine",
        "Wales"]

    const makeItem = (x) => {
        return <option key={x}>{x}</option>
    }
    const getUserScorerPrediction = async (userId) => {
        var pred
        await db.ref("user-predictions/" + userId + "/topscorer").once("value", (snapshot) => {
            if (snapshot.val()) {
                pred = snapshot.val()
            } else {
                pred = null
            }
        })
        return setScorePrediction(pred)
    }
    const updateUserScorePrediction = async (userId, score) => {
        await db.ref("user-predictions/" + userId + "/topscorer").set(score)

    }
    async function scorerHandler(e) {
        e.preventDefault()
        const scorer = scorerRef.current.value
        await updateUserScorePrediction(props.uid, scorer).then(() => {
            getUserScorerPrediction(props.uid)
        })
    }
    const getUserPrediction = async (userId) => {
        var pred
        await db.ref("user-predictions/" + userId + "/winner").once("value", (snapshot) => {
            if (snapshot.val()) {
                pred = snapshot.val()
            } else {
                pred = null
            }
        })
        return setPrediction(pred)
    }
    const updateUserPrediction = async (userId, winner) => {

        await db.ref("user-predictions/" + userId + "/winner").set(winner)

    }
    async function winnerHandler(e) {
        const win = winnerRef.current.value
        await updateUserPrediction(props.uid, win).then(() => {
            getUserPrediction(props.uid)
        })
    }

    useEffect(() => {
        getUserPrediction(props.uid)
        getUserScorerPrediction(props.uid)
    }, [props.uid])


    return (
        <>
            <div className="col-6">
                {firstMatch > newDate &&
                    <form>
                        <span className="text-light">Select winning team: </span>
                        <select onChange={(event) => winnerHandler(event)} ref={winnerRef}>{teams.map(makeItem)}</select>
                    </form>
                        }{prediction &&
                    <p className="text-light">You have selected <b>{prediction}</b> as the winning team</p>}
            </div>
            <div className="col-6">
            {firstMatch > newDate &&
                <form onSubmit={scorerHandler}>
                    <label className="text-light">Top scorer:</label>
                    <input type="text" ref={scorerRef}></input>
                    <button type="submit">Save</button>
                </form>
            }{scorePrediction &&
                <p className="text-light">Top scorer prediction: <b>{scorePrediction}</b></p>}
            </div>
            
        </>
    )

}


export default WinnerPrediction