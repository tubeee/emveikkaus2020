import React, { useEffect, useState } from 'react'
import { useAuth } from '../contexts/AuthContext.js'
import { db } from '../fire.js'
import Header from './Header.js'
import Menu from './Menu.js'
import PredictionForm from './PredictionForm.js'
import WinnerPrediction from './WinnerPrediction.js'

const Match = (props) => {
    const { currentUser } = useAuth()
    const [match, setMatch] = useState()
    const [userPredictions, setUserPredictions] = useState()
    const matchRef = db.ref("matches/" + props.matchId)
    const userPredRef = db.ref("user-predictions/" + props.matchId)
    
    async function getUsers() {
        var user
        await db.ref("users/")
            .once("value", snapshot => {
                user = snapshot.val()
            })
        return user
    }
    const getMatch = async()=> {
        await matchRef.once("value", (snapshot) => {
            let newState = []
            snapshot.forEach(function (childSnapshot) {
                newState.push({
                    homeTeam: childSnapshot.val().homeTeam,
                    awayTeam: childSnapshot.val().awayTeam,
                    dateTime: childSnapshot.val().dateTime,
                    result: childSnapshot.val().result,
                })
            })
            return setMatch(newState)
        })


    }
    const getUserPrediction = async (userId) => {
        var prediction
        await db.ref("user-predictions/" + userId +"/" + props.matchId).once("value", (snapshot) => {
            if (snapshot.val()) {
                prediction = snapshot.val()
            } else {
                prediction = null
            }
        })
        return prediction
    }
        
    }
    return (
        <div></div>
    )
}


export default Match