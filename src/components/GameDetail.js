import React, { useEffect, useState } from 'react'
import { useAuth } from '../contexts/AuthContext.js'
import { db } from '../fire.js'
import Header from './Header.js'
import Menu from './Menu.js'
import PredictionForm from './PredictionForm.js'
import WinnerPrediction from './WinnerPrediction.js'

const GameDetail = () => {

    const { currentUser } = useAuth()
    const [games, setGames] = useState({})
    const [countries, setCountries] = useState({})
    const [admin, setAdmin] = useState(false)
    const [loading, setLoading] = useState(false)
    const [predictions, setPredictions] = useState({})
    const matchRef = db.ref("matches/")
    const userPredictionRef = db.ref("user-predictions/" + currentUser.uid)
    const countryRef = db.ref("countries/")


    useEffect(() => {
        async function getMatchData() {
            await matchRef.orderByChild("dateTime").once("value", (snapshot) => {
                let newState = []
                snapshot.forEach(function (childSnapshot) {
                    newState.push({
                        id: childSnapshot.key,
                        homeTeam: childSnapshot.val().homeTeam,
                        awayTeam: childSnapshot.val().awayTeam,
                        dateTime: childSnapshot.val().dateTime,
                        result: childSnapshot.val().result,
                        finished: childSnapshot.val().finished,
                    })
                })
                return setGames(newState)
                //setLoading(true)
            })
        }
        const checkIsAdmin = async () => {
            return await db.ref("users/" + currentUser.uid).once("value", (snapshot) => {
                setAdmin(snapshot.val())
                setLoading(true)
            })
        }

        const getCountryData = async () => {
            return await countryRef.once("value", (snapshot) => {
                setCountries(snapshot.val())
                setLoading(true)
            })
        }

        const getPredictionData = async () => {
            userPredictionRef.on("value", (snapshot) => {
                setPredictions(snapshot.val())
                setLoading(true)
            })
        }
        getMatchData()
        getPredictionData()
        getCountryData()
        checkIsAdmin()
        setLoading(true)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (!loading) {
        return (
            <div>Loading</div>
        )
    } else {
        return (
            <>
                <Header />

                <div className="container">
                    <div className="row">
                        <WinnerPrediction uid={currentUser.uid} />
                        {Object.keys(games).map((item, i) => (
                            <>
                                {!games[item].result &&
                                    <div key={i} className="col-lg-6 mb-4">
                                        {games[item].homeTeam in countries && games[item].awayTeam in countries &&
                                            <div className="bg-light p-4 rounded">
                                                <div className="">
                                                    <div className="d-flex align-items-center justify-content-around justify-content-between w-100">
                                                        <div className="text-center">
                                                            <img className="img-responsive col-2" src={countries[games[item].homeTeam].img} alt="" />
                                                            <h3>{games[item].homeTeam}</h3>
                                                        </div>
                                                        <div>
                                                            <span>VS</span>
                                                        </div>
                                                        <div className="text-center">
                                                            <img className="img-responsive col-2" src={countries[games[item].awayTeam].img} alt="" />
                                                            <h3>{games[item].awayTeam}</h3>
                                                        </div>
                                                    </div>
                                                    <div className="text-center mb-4">
                                                        <span className="mb-5">
                                                            <span className="d-block">{new Date(games[item].dateTime * 1000).toLocaleString()}
                                                            </span>
                                                            {games[item].result &&
                                                                <h2 className="d-block text-primary">{games[item].result.home} - {games[item].result.away}</h2>
                                                            }
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="product-content">
                                                    {predictions != null && games[item].id in predictions ?
                                                        <PredictionForm className="" key={i} date={games[item].dateTime} finish={games[item].finished} admin={admin} uid={currentUser.uid} matchId={games[item].id} homeTeam={games[item].homeTeam} awayTeam={games[item].awayTeam} home={predictions[games[item].id].result.home} away={predictions[games[item].id].result.away} />
                                                        :
                                                        <PredictionForm className="" key={i} date={games[item].dateTime} finish={games[item].finished} admin={admin} uid={currentUser.uid} matchId={games[item].id} homeTeam={games[item].homeTeam} awayTeam={games[item].awayTeam} />
                                                    }
                                                </div>
                                            </div>
                                        }
                                    </div>
                                }
                            </>
                        ))}
                    </div>
                </div>
            </>
        )
    }
}


export default GameDetail