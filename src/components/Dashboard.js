import React from 'react'
import { Card, Button, Container } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext.js'
import { Link } from 'react-router-dom'
import Header from './Header'
import Menu from './Menu'

export default function Dashboard() {
    const { currentUser } = useAuth()
    return (
        <>
            <Header />
            <Container>
                <Card>
                    <Card.Body>
                        <h2>Profile</h2>
                        <div><strong>Email: </strong>{currentUser.email}</div>
                        <div><strong>Nickname: </strong>{currentUser.displayName}</div>
                        <Button>
                            <Link to="/update-profile" className="btn w-100">Update Profile</Link>
                        </Button>
                    </Card.Body>
                </Card>
                <div className="w-100 text-center mt-2">
                </div>
            </Container>
        </>
    )
}