import React, { useState } from 'react'
import { useAuth } from '../contexts/AuthContext.js'
import { Link, useHistory } from 'react-router-dom'
import { Alert } from 'bootstrap'

const Menu = () => {

    const [error, setError] = useState("")
    const { currentUser, logout } = useAuth()
    const history = useHistory()
    //console.log(currentUser.uid)
    async function handleLogout() {
        setError("")
        try {
            await logout()
            history.push("/login")
        } catch {
            setError("Failed to logout")
        }
    }
    let stylez = {

    }
    return (
        <nav className="navbar navbar-expand">
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav ml-auto flex-wrap">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">
                            Home
                        </Link>
                    </li>
                    {currentUser &&
                        <li className="nav-item">
                            <Link className="nav-link" to="/matches">
                                Matches
                            </Link>
                        </li>
                    }
                    {currentUser &&
                        <li className="nav-item">
                            <Link className="nav-link" to="/results">
                                Results
                            </Link>
                        </li>
                    }
                    {currentUser &&
                        <li className="nav-item">
                            <Link className="nav-link" to="/leaderboard">
                                Leaderboard
                            </Link>
                        </li>
                    }
                    {currentUser &&
                        <li className="nav-item">
                            <Link className="nav-link" to="/dashboard">
                                Dashboard
                            </Link>
                        </li>
                    }
                    <li className="nav-item">
                        {!currentUser &&
                            <Link className="nav-link" to="/login">
                                Login
                            </Link>
                        }
                        {currentUser &&
                            <Link className="nav-link" to="/" onClick={handleLogout}>
                                Logout
                            </Link>
                        }
                        {error &&
                            <Alert>{error}</Alert>}
                    </li>
                </ul>
            </div>
        </nav>
        // <nav className="navbar navbar-expand-sm py-0 px-0 mb-3 ">
        //     <div className="navbar-collapse justify-content-center">
        //         <ul className="nav navbar-nav">
        //             <li className="nav-item">
        //                 <Link className="nav-link" to="/">
        //                     Home
        //                 </Link>
        //             </li>
        //             {currentUser &&
        //                 <li className="nav-item">
        //                     <Link className="nav-link" to="/matches">
        //                         Matches
        //                 </Link>
        //                 </li>
        //             }
        //             {currentUser &&
        //                 <li className="nav-item">
        //                     <Link className="nav-link" to="/results">
        //                         Results
        //                 </Link>
        //                 </li>
        //             }
        //             {currentUser &&
        //                 <li className="nav-item">
        //                     <Link className="nav-link" to="/leaderboard">
        //                         Leaderboard
        //                 </Link>
        //                 </li>
        //             }
        //             {currentUser &&
        //                 <li className="nav-item">
        //                     <Link className="nav-link" to="/dashboard">
        //                         Dashboard
        //                 </Link>
        //                 </li>
        //             }
        //             <li className="nav-item">
        //                 {!currentUser &&
        //                     <Link className="nav-link" to="/login">
        //                         Login
        //                 </Link>
        //                 }
        //                 {currentUser &&
        //                     <Link className="nav-link" to="/" onClick={handleLogout}>
        //                         Logout
        //                 </Link>
        //                 }
        //                 {error &&
        //                     <Alert>{error}</Alert>}
        //             </li>
        //         </ul>
        //     </div>
        // </nav>
    )
}

export default Menu