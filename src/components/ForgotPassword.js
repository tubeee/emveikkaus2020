import React, { useState, useRef } from 'react'
import Header from './Header'
import Menu from './Menu'
import { Card, Form, Button, Alert } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext.js'
import { Link } from 'react-router-dom'

export default function ForgotPassword() {
  const emailRef = useRef()
  const { resetPassword } = useAuth()
  const [error, setError] = useState("")
  const [message, setMessage] = useState("")
  const [loading, setLoading] = useState(false)

  async function handleSubmit(e) {
    e.preventDefault()
    try {
      setMessage("")
      setError("")
      setLoading(true)
      await resetPassword(emailRef.current.value)
      setMessage("Check your email")
    } catch {
      setError("Failed to reset password")
    }
    setLoading(false)
  }
  return (
    <>
      <Header />
      <Card>
        <Card.Body>
          <h2 className="text-center mb-4">Password reset</h2>
          {error && <Alert style={{ color: "red" }}>{error}</Alert>}
          {message && <Alert style={{ color: "green" }}>{message}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" ref={emailRef} required />
            </Form.Group>
            <Button disabled={loading} className="w-100" type="submit">Reset Password</Button>
          </Form>
          <div>
            <Link to="/login">Login</Link>
          </div>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        Dont have an account? <Link to="/signup">Sign Up!</Link>
      </div>
    </>
  )
}
