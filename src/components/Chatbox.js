import React, { useEffect, useState } from 'react'

import { useAuth } from '../contexts/AuthContext.js'
import { db } from '../fire.js'
import ChatMessage from './ChatMessage.js'

const Chatbox = () => {
    const { currentUser } = useAuth()
    const messagesRef = db.ref("messages")
    const [messages, setMessages] = useState()
    const [formValue, setFormValue] = useState("")

    const getMessages = async () => {
        await messagesRef.orderByChild("createdAt").limitToLast(10).once("value", (snapshot) => {
            let newState = []
            console.log(snapshot.val())
            snapshot.forEach(function (childSnapshot) {
                newState.push({
                    id: childSnapshot.val().id,
                    uid: childSnapshot.val().uid,
                    displayName: childSnapshot.val().displayName,
                    text: childSnapshot.val().text,
                    time: childSnapshot.val().createdAt
                })
            })
            newState.reverse()
            return setMessages(newState)
        })
    }
    const sendMessage = async (e) => {
        e.preventDefault()
        const { uid } = currentUser
        await messagesRef.push({
            text: formValue,
            createdAt: (Math.floor(new Date() / 1000)),
            displayName: currentUser.displayName,
            uid
        }).then(() => {
            getMessages()
            setFormValue("")
        })
    }
    useEffect(() => {
        getMessages()
    }, [])
    const divStyle = {
        overflowY: 'scroll',
        height: '500px',

    }
    return (
        <div className="d-flex justify-content-center">
            <div className="card col-md-8 col-xl-6">

                <div style={divStyle}>
                    {messages && messages.map(msg => <ChatMessage key={msg.id} message={msg} />)}
                </div>
                {currentUser &&
                    <div className="panel-footer">
                        <form className="input-group" onSubmit={sendMessage}>
                            <input className="form-control input-sm" value={formValue} onChange={(e) => setFormValue(e.target.value)} placeholder="Type your message here" />
                            <span className="input-group-btn"><button className="btn btn-warning btn-sm" type="submit" disabled={!formValue}>Send</button></span>
                        </form>
                    </div>
                }
            </div>
        </div>
    )
}
export default Chatbox