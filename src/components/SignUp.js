import React, { useState, useRef } from 'react'
import Header from './Header'
import Menu from './Menu'
import { Card, Form, Button, Alert, Container } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext.js'
import { Link, useHistory } from 'react-router-dom'

const SignUp = () => {
  const emailRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const usernameRef = useRef()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const history = useHistory()
  const { signup } = useAuth()

  async function handleSubmit(e) {
    e.preventDefault()
    setLoading(true)
    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match")
    }
    try {
      setError("")
      await signup(emailRef.current.value, passwordRef.current.value, usernameRef.current.value).then(()=>{
        setLoading(false)
      })
      history.push("/")
    } catch {
      setError("Failed to sign up")
    }
  }
  return (
    <>
      <Header />
      <Container className="col-6">
        <Card>
          <Card.Body>
            <h2 className="text-center mb-4">Sign Up</h2>
            {error && <Alert style={{ color: "red" }}>{error}</Alert>}
            <Form onSubmit={handleSubmit}>
            <Form.Group id="username">
                <Form.Label>Username</Form.Label>
                <Form.Control type="text" ref={usernameRef} required />
              </Form.Group>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" ref={emailRef} required />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" ref={passwordRef} required />
              </Form.Group>
              <Form.Group id="password-confirm">
                <Form.Label>Password Confirmation</Form.Label>
                <Form.Control type="password" ref={passwordConfirmRef} required />
              </Form.Group>
              <Button disabled={loading} className="w-100" type="submit">Sign Up</Button>
            </Form>
          </Card.Body>
        </Card>

        <div className="w-100 text-center mt-2">
          Already have an account? <Link to="login">Log In!</Link>
        </div>
      </Container>
    </>
  )
}

export default SignUp