import React, { useEffect, useState } from 'react'
import { useAuth } from '../contexts/AuthContext.js'
import Menu from './Menu'
const Header = () => {

    const { currentUser } = useAuth()
    const [date, setDate] = useState(new Date())

    useEffect(() => {
        var timerID = setInterval(() => tick(), 1000);

        return function cleanup() {
            clearInterval(timerID);
        };
    });
    const tick = () => {
        setDate(new Date())
    }
    const options = { month: 'long' }
    return (
        <>
            <div className="header-bottom">
                <div className="container">
                    <div className="row">
                        <div className="col-xl-3 col-lg-3 d-xl-block d-lg-block align-items-center d-none">
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 d-xl-flex d-lg-flex d-flex">
                                    <div className="left-area ">
                                        <ul>
                                            <li>
                                                <span className="text-light">
                                                    <span>{date.getDate()} </span>
                                                    <span> {new Intl.DateTimeFormat('en-US', options).format(date)} </span>
                                                    <span> {date.getFullYear()}</span>
                                                </span>
                                                <span>&mdash;</span>
                                                <span className="text-light">
                                                    <span>{date.getHours()}</span>:
                                                    <span>{(date.getMinutes() < 10 ? '0' : '') + date.getMinutes()}</span>:
                                                    <span>{(date.getSeconds() < 10 ? '0' : '') + date.getSeconds()}</span>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-9 col-lg-9 ">
                            <Menu />
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-100 d-flex align-items-center justify-content-center" style={{ backgroundImage: `url(/static/arena.jpg)`, backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat' }}>
                <div className="">
                    <div>
                        <div className="center-block">
                            <img className="img-fluid" src="/static/logot/UEFA-Euro-2020.png" alt="" />
                        </div>
                        {currentUser && currentUser.displayName && <h6>Logged in as: {currentUser.displayName}</h6>}
                        {currentUser && !currentUser.displayName && <h6>Logged in as: {currentUser.email}</h6>}
                    </div>
                </div>


            </div>
        </>
    )
}
export default Header