import React from 'react'
import Chatbox from './Chatbox'
import Header from './Header'
import Menu from './Menu'

const index = () => {
    return (
        <div>
            <Header />
            <div className="container">
                <div className="w-100 d-flex align-items-center justify-content-center">
                    <h2 className="text-light">
                        Welcome to the Euro2020 score prediction app.
                    </h2>

                </div>
                
                <div className="container text-light">
                    <h3>FAQ:</h3>
                    <h5>How are the points calculated?</h5>
                    <p>You get 3 points for the correct outcome and 1 point for each correct score. So the maximum amount of points per game is 5.<br />
                    Example: Team A won Team B by 3-2 and your prediction was 3-1. You get 3+1=4 points as the outcome and Team A goal amount was correct.</p>
                    <h5>What do I put as my prediction if I think the game will end up as a draw?</h5>
                    <p>Final result is the result after regulation time (90mins). No overtime periods will be played in the group matches but if a playoff match ends as a draw after regulation time, an overtime period will be played but the match outcome will be counted as draw</p>
                </div>
                <Chatbox/>
            </div>
        </div>
    )
}

export default index